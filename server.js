const express = require("express");
const fallback = require("express-history-api-fallback");
const compression = require("compression");

const app = express();
const port = process.env.PORT || 8080;

app.use(compression());

app.use(express.static(`${__dirname}/dist`));

app.use(fallback(`${__dirname}/dist/index.html`));

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
