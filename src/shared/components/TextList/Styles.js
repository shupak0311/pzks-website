import styled from "styled-components";

export const HeaderText = styled.p`
  font-size: 1rem;
  font-weight: normal;
  line-height: 1.6;
  margin-bottom: 1.25rem;
  text-rendering: optimizeLegibility;
`;

export const List = styled.ul`
  list-style: disc;
  margin-left: 1.1rem;
  margin-bottom: 1.25rem;
`;
