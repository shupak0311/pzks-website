import React, { Fragment } from "react";
import * as S from "./Styles";

const TextList = ({ headerText, items }) => (
  <Fragment>
    <S.HeaderText>
      <strong>{headerText}</strong>
    </S.HeaderText>
    <S.List>
      {items.map((item, index) => (
        <li key={index}>
          <strong>{item}</strong>
        </li>
      ))}
    </S.List>
  </Fragment>
);

export default TextList;
