import React, { Fragment } from "react";
import { Container, Content, Columns } from "./Styles";

const Wrapper = ({ children }) => {
  return (
    <Fragment>
      <Container>
        <Content>
          <Columns>{children}</Columns>
        </Content>
      </Container>
    </Fragment>
  );
};

export default Wrapper;
