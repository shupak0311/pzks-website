import styled from "styled-components";


export const Container = styled.div`
  width: 100%;
  /* padding: 0 3%;  media query*/ 
  max-width: 62.5rem;
  margin: 0 auto;
`

export const Content = styled.div`
    height: 100%;
    display: block;
    padding: 20px 0;
`

export const Columns = styled.div`
  position: relative;
  padding-right: .9375rem;
`
