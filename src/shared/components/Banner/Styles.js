import styled from "styled-components";

export const Banner = styled.div`
  position: relative;
  width: 100%;
  height: 500px;
  /* padding: 0 5%; */
  overflow: hidden;
  backface-visibility: hidden;
`;

export const BannerBackground = styled.span`
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
  z-index: -1;
  background: black
    url('${props => props.backgroundImg}')
    no-repeat center center;
  background-size: cover;
`;

export const BannerTextContainer = styled.div`
  color: white;
  display: block;
  margin: 0 auto;
  max-width: 62.5rem;
  margin-top: 150px;
  text-shadow: 0 1px 10px rgba(0, 0, 0, 0.3);
`;

export const Columns = styled.div`
  position: relative;
  /*media query */
  /* padding-left: 0.9375rem;
  padding-right: 0.9375rem; */
`;

export const BannerTextPrimary = styled.h2`
  font-size: 40px;
`;

export const BannerTextSecondary = styled.h3`
  font-size: 18px;
  margin-bottom: 12px;
`;
