import React from "react";

import * as S from "./Styles";

const Banner = ({ title, subtitle, backgroundImg }) => (
  <S.Banner>
    <S.BannerBackground backgroundImg={backgroundImg} />
    <S.BannerTextContainer>
      <S.Columns>
        <S.BannerTextSecondary>{title}</S.BannerTextSecondary>
        <S.BannerTextPrimary>{subtitle}</S.BannerTextPrimary>
      </S.Columns>
    </S.BannerTextContainer>
  </S.Banner>
);

export default Banner;
