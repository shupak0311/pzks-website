import React from "react";
import * as S from "./Styles";

const Title = ({ title, small = false }) => (
  <S.Title small={small}>{title}</S.Title>
);

export default Title;
