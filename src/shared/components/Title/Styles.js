import styled from "styled-components";

export const Title = styled.h2`
  font-size: ${props => (props.small ? "1.5rem" : "2rem")};
  font-weight: bold;
  color: #081e3f;
  margin-bottom: 0.5rem;
`;
