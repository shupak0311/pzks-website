import React, { Fragment } from "react";
import { useRouteMatch } from "react-router-dom";
import { parseURL } from "../../utils/helpers";

import { Breadcrumb } from 'semantic-ui-react'


const sections = [
    { key: 'Home', content: 'Головна', link: true, href:'/'},
    { key: 'Overview', content: 'Загальна інформація', link: true, href:'/overview', active: true},
  ]

const Breadcrumbs = () => {
  const match = useRouteMatch();
  const breadcrumbs = parseURL(match.path);

  return (
    <Breadcrumb icon='right angle' sections={sections} style={{ marginBottom: "12px" }}/>
  );
};

export default Breadcrumbs;
