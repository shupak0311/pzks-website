import styled from "styled-components";



export const TeacherCardBox = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
`;

export const TeacherCardInner = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  padding: 0 20px 20px;
  text-decoration: none;
  cursor: pointer;
`;

export const TeacherImg = styled.div`
  height: 192px;
  background-image: url(${props => props.url});
  background-size: cover;
  margin-bottom: 5px;
`;

export const TeacherName = styled.span`
  margin-bottom: 4px;
  color: #121314;
  font-weight: 500;
  font-size: 16px;
  line-height: 18px;
`;

export const TeacherPosition = styled.span`
  color: #121314;
  font-size: 14px;
`;

export const TeacherCardBottom = styled.div`
  position: relative;
  z-index: 2;
  visibility: hidden;
  padding: 0 20px 20px;
  background-color: #fff;
  opacity: 0;
`;

export const TeacherCard = styled.div`
  display: flex;
  flex-direction: column;
  width: 20%;
  height: 255px;
  margin: 20px 0 0 0;
  position: relative;
  overflow: hidden;
  &:hover {
    transform: translateY(-20px);
    background-color: #fff;
    overflow: visible;
    z-index: 1;
  }
  &:hover ${TeacherCardBox} {
    box-shadow: 0 10px 50px 0 rgba(115, 115, 115, 0.52);
  }
  &:hover ${TeacherCardInner} {
    padding: 0 0 20px;
    height: auto;
    background-color: #fff;
  }
  &:hover ${TeacherImg} {
    height: 225px;
  }
  &:hover ${TeacherName} {
    padding: 0 20px;
  }
  &:hover ${TeacherPosition} {
    padding: 0 20px;
  }
  &:hover ${TeacherCardBottom} {
    visibility: visible;
    opacity: 1;
  }
`;

export const TeacherLink = styled.a`
  display: inline;
  border-bottom: 1px solid rgba(0, 0, 0, 0.4);
  color: #121314;
  text-decoration: none;
  font-weight: 500;
  font-size: 14px;
  line-height: 18px;
`;