import React from "react";
import * as S from "./Styles";

const TeacherCard = ({ img, name, position, link }) => {
  return (
    <S.TeacherCard>
      <S.TeacherCardBox>
        <S.TeacherCardInner>
          <S.TeacherImg url={img} />
          <S.TeacherName>{name}</S.TeacherName>
          <S.TeacherPosition>{position}</S.TeacherPosition>
        </S.TeacherCardInner>
        <S.TeacherCardBottom>
          <S.TeacherLink target="_blank" href={link}>Сторінка викладача</S.TeacherLink>
        </S.TeacherCardBottom>
      </S.TeacherCardBox>
    </S.TeacherCard>
  );
};

export default TeacherCard;
