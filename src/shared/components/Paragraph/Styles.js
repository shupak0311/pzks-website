import styled from "styled-components";

export const Chunk = styled.p`
  margin-bottom: 1.25rem;
  color: #222;

  &:last-of-type {
    margin-bottom: 0;
  }
`;
