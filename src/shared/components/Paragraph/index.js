import React from "react";
import * as S from "./Styles";

const Paragraph = ({ chunks }) =>
  chunks.map((chunk, index) => <S.Chunk key={index}>{chunk}</S.Chunk>);

export default Paragraph;
