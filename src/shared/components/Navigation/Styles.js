import styled from "styled-components";

export const Navigation = styled.div`
padding-right: 15px;
width: 25%;
display: inline-block;
`;

export const NavList = styled.ul`
display: flex;
flex-direction: column;
`;

export const ListItem = styled.li`
font-size: 0.875rem;
font-weight: normal;
border-bottom: ${props => (props.active ? "none" : `1px solid #ddd`)};
transition: ease background-color 0.2s;
background-color: ${props => (props.active ? "#081E3f" : "#fff")};
color: ${props => (props.active ? "#fff" : "#081E3f")};
cursor: pointer;

&:hover {
  background-color: ${props => props.active ? "" : "#f1f1f1"}
}

&:last-of-type {
  border-bottom: none;
}
`;

export const Link = styled.a`
padding: 15px 20px;
display: block;
`;

export const Text = styled.span`
  display: block;
`;