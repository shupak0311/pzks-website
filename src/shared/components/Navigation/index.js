import React from "react";
import * as S from "./Styles";
import { useRouteMatch, NavLink } from "react-router-dom";

const Navigation = () => {
  const { path } = useRouteMatch();

  const items = [
    {
      placeholder: "Загальна інформація",
      url: "/overview"
    },
    {
      placeholder: "Контакти",
      url: "/contacts"
    },
    {
      placeholder: "Кадровий склад",
      url: "/staff"
    }
  ];

  return (
    <S.Navigation>
      <S.NavList>
        {items.map(({ placeholder, url }, index) => {
          const active = path === url;
          const linkProps = { as: NavLink, exact: true, to: url };

          return (
            <S.ListItem active={active} key={index}>
              <S.Link {...linkProps}>
                <S.Text>{placeholder}</S.Text>
              </S.Link>
            </S.ListItem>
          );
        })}
      </S.NavList>
    </S.Navigation>
  );
};

export default Navigation;
