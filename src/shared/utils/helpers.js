export const parseURL = (url) => url.split("/");

export const findPos = (obj) => {
  let curTop = 0;
  if (obj.offsetParent) {
    do {
      curTop += obj.offsetTop;
    } while ((obj = obj.offsetParent));
    return [curTop];
  }
};
