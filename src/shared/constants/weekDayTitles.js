export const weekDayTitles = {
  "1": (abbreviated = false) => (abbreviated ? "пн" : "понеділок"),
  "2": (abbreviated = false) => (abbreviated ? "вт" : "вівторок"),
  "3": (abbreviated = false) => (abbreviated ? "ср" : "середа"),
  "4": (abbreviated = false) => (abbreviated ? "чт" : "четвер"),
  "5": (abbreviated = false) => (abbreviated ? "пт" : "п'ятниця"),
  "6": (abbreviated = false) => (abbreviated ? "сб" : "субота"),
  "7": (abbreviated = false) => (abbreviated ? "нд" : "неділя"),
};
