import React, { Fragment } from "react";
import { Flex, Link } from "../../Styles";
import Wrapper from "@Components/Wrapper";
import Title from "@Components/Title/index";
import Breadcrumbs from "@Components/Breadcrumbs";
import { Icon, List, Segment, Statistic } from "semantic-ui-react";
import {
  Container as SemanticContainer,
  Divider,
  Message,
} from "semantic-ui-react";

const PreparatoryCourses = () => {
  return (
    <Fragment>
      <Wrapper>
        <Breadcrumbs />
        <Title title={"Підготовчі курси"} />

        <SemanticContainer textAlign="left">
          <p>
            На факультеті Прикладної математики діють підготовчі курси (філія
            факультету доуніверситетської підготовки "
            <Link href="http://kpi.ua/">КПІ ім. Ігоря Сікорського</Link>").
          </p>
          <Segment raised color="blue" style={{ marginTop: "20px" }}>
            <p>Робота підготовчик курсів спрямована на:</p>
            <List ordered>
              <List.Item>
                Підготовку до зовнішнього незалежного оцінювання знань з
                конкурсних дисциплін.
              </List.Item>
              <List.Item>
                Приведення рівня знань слухачів до вимог університетських
                програм.
              </List.Item>
              <List.Item>Поглиблену професійну орієнтацію слухачів.</List.Item>
            </List>
          </Segment>

          <p style={{ marginTop: "20px" }}>
            Організацію та координацію роботи підготовчих курсів здійснює
            Інститут моніторингу якості освіти "
            <Link href="http://kpi.ua/">КПІ ім. Ігоря Сікорського</Link>". По
            закінченню підготовчих курсів слухачам видаються Свідоцтва.
          </p>

          <p>
            ЩОРІЧНО СТУДЕНТАМИ "
            <Link href="http://kpi.ua/">КПІ ім. Ігоря Сікорського</Link>" СТАЮТЬ
            БЛИЗЬКО{" "}
            <Statistic size="tiny">
              <Statistic.Value>95%</Statistic.Value>
            </Statistic>{" "}
            ВИПУСКНИКІВ ПІДГОТОВЧИХ КУРСІВ ФПМ.
          </p>

          <Divider />

          <p style={{ marginTop: "20px" }}>
            <strong>До послуг слухачів:</strong>
            <List bulleted>
              <List.Item>
                очна форма навчання - заняття 3 рази на тиждень - для киян та
                мешканців регіону;
              </List.Item>
              <List.Item>
                екстернат - заняття по суботах (гнучке поєднання денної та
                заочної форм навчання) - для слухачів незалежно від місця
                проживання.
              </List.Item>
            </List>
          </p>

          <Divider />

          <Flex style={{ marginTop: "20px" }}>
            <Icon name="paste" size="large" />
            Прийом документів - друга декада вересня поточного року.
          </Flex>

          <Flex style={{ marginTop: "20px" }}>
            <Icon name="location arrow" size="large" />
            Прийом документів: м. Київ-56, вул. Політехнічна, навчальний корпус
            № 15, кімната № 105.
          </Flex>

          <Flex style={{ marginTop: "20px" }}>
            <Icon name="phone" size="large" />
            Довідки за телефонами: 204-81-15, 204-91-13.{" "}
          </Flex>

          <Message>
            <Message.Header>До заяви слухачі додають:</Message.Header>
            <Message.List>
              <Message.Item>4 фото (3х4);</Message.Item>
              <Message.Item>
                ксерокопію паспорта або свідоцтва про народження;
              </Message.Item>
              <Message.Item>5 файлів;</Message.Item>
              <Message.Item>6 зошитів.</Message.Item>
            </Message.List>
          </Message>
        </SemanticContainer>
      </Wrapper>
    </Fragment>
  );
};

export default PreparatoryCourses;
