import React, { Fragment } from "react";
import Title from "@Components/Title/";
import Breadcrumbs from "@Components/Breadcrumbs";
import Wrapper from "@Components/Wrapper";
import { List } from "semantic-ui-react";
import TimeLine from "./Timeline";
import {
  Container as SemanticContainer,
  Divider,
  Message,
  Header,
  Segment,
  Table,
} from "semantic-ui-react";

const BachelorProgram = () => {
  return (
    <Fragment>
      <Wrapper>
        <Breadcrumbs />
        <Title title={"Вступ на перший курс"} />

        <SemanticContainer textAlign="left">
          <div>
            <Header as="h4" color="blue">
              ОСВІТНЯ ПРОГРАМА “ІНЖЕНЕРІЯ ПРОГРАМНОГО ЗАБЕЗПЕЧЕННЯ КОМП’ЮТЕРНИХ
              ТА ІНФОРМАЦІЙНО-ПОШУКОВИХ СИСТЕМ” (СПЕЦІАЛЬНІСТЬ 121 ІНЖЕНЕРІЯ
              ПРОГРАМНОГО ЗАБЕЗПЕЧЕННЯ)
            </Header>
            <SemanticContainer attached>
              Зміст діяльності фахівців з ІНЖЕНЕРІЇ ПРОГРАМНОГО ЗАБЕЗПЕЧЕННЯ за
              освітньою програмою Інженерія програмного забезпечення
              комп’ютерних та інформаційно-пошукових систем полягає у
              розробленні прикладного програмного забезпечення комп’ютерних
              систем та мереж у вигляді корпоративних систем, систем підтримки
              прийняття рішень, інтелектуальних систем, програмних продуктів для
              бізнесу, мультимедійного програмного забезпечення, web-додатків та
              мобільних додатків, баз даних та знань, спрямованого на вирішення
              широкого кола прикладних задач.
            </SemanticContainer>
          </div>

          <Segment raised color="blue" style={{ marginTop: "20px" }}>
            <Header as="h4">
              Інженерія програмного забезпечення комп’ютерних та
              інформаційно-пошукових систем
            </Header>
            <List bulleted>
              <List.Item>
                Прикладне програмне забезпечення комп’ютерних систем.
              </List.Item>
              <List.Item>
                Об’єктно-орієнтовані інженерні методи проектування та
                конструювання складних програмних систем
              </List.Item>
              <List.Item>
                Програмне забезпечення систем інтелектуального пошуку інформації
                (Information Retrieval, Data Mining, Text Mining), програмні
                методи ефективного оброблення, зберігання та захисту даних
                великих обсягів (Big Data), в тому числі мультимедійних даних
                (Multimedia та Mulsemedia).{" "}
              </List.Item>
            </List>
          </Segment>

          <Divider />

          <Header as="h3">Перелік ЗНО для спеціальності:</Header>

          <Table fixed>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Предмет ЗНО</Table.HeaderCell>
                <Table.HeaderCell>
                  Вага предметів сертифікату ЗНО
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Вага свідоцтва про здобуття повної загальної середньої освіти
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Вага бала за успішне закінчення підготовчих курсів ЗВО
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Мінімальний конкурсний бал для допуску до участі в конкурсі
                  або для зарахування на навчання за квотою
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              <Table.Row>
                <Table.Cell>Українська мова та література</Table.Cell>
                <Table.Cell>0,2</Table.Cell>
                <Table.Cell>0</Table.Cell>
                <Table.Cell>0</Table.Cell>
                <Table.Cell>125</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Математика</Table.Cell>
                <Table.Cell>0,5</Table.Cell>
                <Table.Cell>0</Table.Cell>
                <Table.Cell>0</Table.Cell>
                <Table.Cell>125</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Фізика або іноземна мова*</Table.Cell>
                <Table.Cell>0,25</Table.Cell>
                <Table.Cell>0</Table.Cell>
                <Table.Cell>0</Table.Cell>
                <Table.Cell>125</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
          <Segment tertiary>
            *У 2020 році приймаються сертифікати зовнішнього незалежного
            оцінювання 2017, 2018, 2019 та 2020 років, крім оцінок з
            англійської, французької, німецької та іспанської мов, результати
            яких приймаються лише із сертифікатів зовнішнього незалежного
            оцінювання 2018 - 2020 років.
          </Segment>

          <Divider />

          <Header as="h4">
            Розклад роботи Приймальної комісії КПІ ім. Ігоря Сікорського під час
            прийому заяв та документів
          </Header>

          <Table celled structured>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Термін</Table.HeaderCell>
                <Table.HeaderCell>Дні</Table.HeaderCell>
                <Table.HeaderCell>Години</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              <Table.Row>
                <Table.Cell rowSpan="3">6 травня – 2 червня</Table.Cell>
                <Table.Cell>Понеділок – четвер</Table.Cell>
                <Table.Cell textAlign="right">14:00 – 17:00</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>П'ятниця</Table.Cell>
                <Table.Cell textAlign="right">10:00 – 13:00</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Субота – неділя</Table.Cell>
                <Table.Cell textAlign="right">Вихідний</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell>3 червня</Table.Cell>
                <Table.Cell>Середа</Table.Cell>
                <Table.Cell textAlign="right">14:00 – 18:00</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell>5 липня</Table.Cell>
                <Table.Cell>Неділя</Table.Cell>
                <Table.Cell textAlign="right">14:00 – 18:00</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell rowSpan="3">6 – 12 липня</Table.Cell>
                <Table.Cell>Понеділок – четвер</Table.Cell>
                <Table.Cell textAlign="right">14:00 – 17:00</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>П'ятниця</Table.Cell>
                <Table.Cell textAlign="right">10:00 – 13:00</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Субота – неділя</Table.Cell>
                <Table.Cell textAlign="right">Вихідний</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell rowSpan="3">13 – 22 липня</Table.Cell>
                <Table.Cell>Понеділок – п'ятниця</Table.Cell>
                <Table.Cell textAlign="right">9:00 – 18:00</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Субота</Table.Cell>
                <Table.Cell textAlign="right">10:00 – 13:00</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Неділя</Table.Cell>
                <Table.Cell textAlign="right">Вихідний</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell>27 – 31 липня</Table.Cell>
                <Table.Cell>Понеділок – п'ятниця</Table.Cell>
                <Table.Cell textAlign="right">9:00 – 18:00</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell>1 – 2 серпня</Table.Cell>
                <Table.Cell>Субота – неділя</Table.Cell>
                <Table.Cell textAlign="right">12:00 – 16:00</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell rowSpan="3">3 – 15 серпня</Table.Cell>
                <Table.Cell>Понеділок, середа, четвер</Table.Cell>
                <Table.Cell textAlign="right">14:00 – 17:00</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Вівторок, п'ятниця, субота</Table.Cell>
                <Table.Cell textAlign="right">10:00 – 13:00</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Неділя</Table.Cell>
                <Table.Cell textAlign="right">Вихідний</Table.Cell>
              </Table.Row>

              <Table.Row>
                <Table.Cell>Святкові дні</Table.Cell>
                <Table.Cell />
                <Table.Cell textAlign="right">Вихідний</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
          <Segment secondary>
            <strong>*Увага: у розкладі можливі зміни</strong>
          </Segment>

          <Divider />

          <Header as="h3">
            Етапи вступної кампанії (вступники на основі повної загальної
            середньої освіти)
          </Header>
          <Segment>
            <TimeLine />
            <Segment tertiary>
              <p>
                * о 18-00 16 липня – для осіб, які вступають на основі
                співбесіди, вступних іспитів, творчих конкурсів; о 18-00 22
                липня – для осіб, які вступають за результатами зовнішнього
                незалежного оцінювання, іспитів, творчих конкурсів, складених з
                01 по 12 липня; вступні іспити, творчі конкурси проводяться в
                кілька потоків з 01 по 12 липня включно (для вступників на місця
                державного та регіонального замовлень за графіком, що має бути
                надісланий до 15 березня для моніторингового огляду до
                Міністерства освіти і науки України). У період з 17 по 22 липня
                можуть проводитись додаткові сесії іспитів, творчих конкурсів
                для вступників, які вступають на місця за кошти фізичних та/або
                юридичних осіб. Механізми реєстрації учасників творчих
                конкурсів, вступних іспитів, їх організація та проведення
                визначаються відповідно до законодавства; співбесіди проводяться
                17 липня;
              </p>
              <p>
                ** оприлюднення списків осіб, рекомендованих до зарахування за
                результатами співбесіди, з повідомленням про отримання чи
                неотримання ними права здобувати вищу освіту за державним
                замовленням, здійснюється не пізніше 12-00 години 20 липня.
                Вступники, які отримали рекомендації, мають виконати вимоги до
                зарахування на місця державного замовлення до 10-00 години 22
                липня, а також подати письмову заяву про виключення заяв на інші
                місця державного замовлення. Зарахування цієї категорії
                вступників за державним замовленням відбувається не пізніше
                15-00 години 22 липня. Заяви зарахованих осіб на інші місця
                державного замовлення виключаються впродовж 22 липня;
              </p>
              <p>
                *** в разі наявності вакантних місць, за рішенням Приймальної
                комісії, терміни можуть бути подовжені.
              </p>
            </Segment>
          </Segment>

          <Divider />
          <Message info>
            <Message.Header>
              ОФІЦІЙНІ ДОКУМЕНТИ знаходяться за цим{" "}
              <a
                style={{ color: "blue" }}
                href="http://pk.kpi.ua/documents/"
                target="_blank"
              >
                посиланням.
              </a>
            </Message.Header>
          </Message>
        </SemanticContainer>
      </Wrapper>
    </Fragment>
  );
};

export default BachelorProgram;
