import React, { Fragment } from "react";
import Wrapper from "@Components/Wrapper";
import Title from "@Components/Title/index";
import Breadcrumbs from "@Components/Breadcrumbs";
import { Header } from "semantic-ui-react";
import { Container as SemanticContainer, Message } from "semantic-ui-react";

const PostgraduateSchool = () => {
  return (
    <Fragment>
      <Wrapper>
        <Breadcrumbs />
        <Title title={"Аспірантура"} />

        <Header as="h4" color="blue" style={{ marginTop: "0" }}>
          ПРОГРАМИ ВСТУПНИХ ВИПРОБУВАНЬ ДЛЯ ВСТУПУ ДО АСПІРАНТУРИ ЗА
          СПЕЦІАЛЬНІСТЮ 121 ІНЖЕНЕРІЯ ПРОГРАМНОГО ЗАБЕЗПЕЧЕННЯ ПІДГОТОВКИ PHD
        </Header>
        <SemanticContainer textAlign="left">
          <Message info>
            <Message.Header>
              Програма комплексного фахового випробування на здобуття ступеня
              доктора філософії знаходиться{" "}
              <a
                style={{ color: "blue" }}
                href="http://pzks.fpm.kpi.ua/faculty/entry/magistracy/PhD_%D0%9F%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%B0_%D0%B2%D1%81%D1%82%D1%83%D0%BF%D0%BD%D0%BE%D0%B3%D0%BE_%D1%96%D1%81%D0%BF%D0%B8%D1%82%D1%83_%D0%A4%D0%9F%D0%9C_2020.pdf"
                target="_blank"
              >
                за цим посиланням
              </a>
              .
            </Message.Header>
          </Message>

          <Message info>
            <Message.Header>
              Програма додаткового фахового випробування на здобуття ступеня
              доктора філософії знаходиться{" "}
              <a
                style={{ color: "blue" }}
                href="http://pzks.fpm.kpi.ua/faculty/entry/magistracy/PhD_%D0%9F%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%B0_%D0%B4%D0%BE%D0%B4%D0%B0%D1%82%D0%BA%D0%BE%D0%B2%D0%BE%D0%B3%D0%BE_%D0%B2%D1%81%D1%82%D1%83%D0%BF%D0%BD%D0%BE%D0%B3%D0%BE_2020.pdf"
                target="_blank"
              >
                за цим посиланням
              </a>
              .
            </Message.Header>
          </Message>
        </SemanticContainer>
      </Wrapper>
    </Fragment>
  );
};

export default PostgraduateSchool;
