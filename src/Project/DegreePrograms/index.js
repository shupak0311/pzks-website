import BachelorProgram from "./BachelorProgram"
import GeneralInfo from "./GeneralInfo"
import Master from "./Master"
import OfficialDocuments from "./OfficialDocuments"
import PostgraduateSchool from "./PostgraduateSchool"
import PreparatoryCourses from "./PreparatoryCourses"

export { BachelorProgram, GeneralInfo, Master, OfficialDocuments, PostgraduateSchool, PreparatoryCourses}