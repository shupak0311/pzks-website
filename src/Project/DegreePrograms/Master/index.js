import React, { Fragment } from "react";
import Wrapper from "@Components/Wrapper";
import Title from "@Components/Title/index";
import Breadcrumbs from "@Components/Breadcrumbs";
import { List, Segment, Header } from "semantic-ui-react";
import { Container as SemanticContainer, Message } from "semantic-ui-react";

const Master = () => {
  return (
    <Fragment>
      <Wrapper>
        <Breadcrumbs />
        <Title title={"Вступ до магістратури"} />

        <SemanticContainer textAlign="left">
          <Segment raised color="blue" style={{ marginTop: "20px" }}>
            <Header as="h4">
              ПРИЙОМ НА КАФЕДРУ ПЗКС ЗА ПРОГРАМАМИ ПІДГОТОВКИ МАГІСТРА
            </Header>{" "}
            <List ordered>
              <List.Item>
                Прийом за програмою підготовки магістра на денну форму навчання
                здійснюється за освітньою програмою “
                <strong>
                  Інженерія програмного забезпечення мультимедійних та
                  інформаційно-пошукових систем
                </strong>
                ” (спеціальність 121 ІНЖЕНЕРІЯ ПРОГРАМНОГО ЗАБЕЗПЕЧЕННЯ).
              </List.Item>
              <List.Item>
                План прийому за держзамовленням (денна форма навчання)
                визначається Міністерством освіти і науки України.
              </List.Item>
              <List.Item>
                Прийом за програмами підготовки <strong>магістра</strong> на
                ПЗКС здійснюється на основі Положення, затвердженого Вченою
                радою факультету.{" "}
              </List.Item>
            </List>
          </Segment>

          <Message info>
            <Message.Header>
              ПРОГРАМА вступного комплексного фахового випробування для вступу
              на освітню програму підготовки магістра «Інженерія програмного
              забезпечення мультимедійних та інформаційно-пошукових систем» за
              спеціальністю «121 Інженерія програмного забезпечення» знаходиться{" "}
              <a
                style={{ color: "blue" }}
                href="http://pzks.fpm.kpi.ua/faculty/entry/magistracy/Prog_IPZ-2020.pdf"
                target="_blank"
              >
                за цим посиланням
              </a>
              .
            </Message.Header>
          </Message>

          <Message info>
            <Message.Header>
              ПРОГРАМА додаткового вступного випробування для вступу на освітню
              програму підготовки магістра «Інженерія програмного забезпечення
              мультимедійних та інформаційно-пошукових систем» за спеціальністю
              «121 Інженерія програмного забезпечення» знаходиться{" "}
              <a
                style={{ color: "blue" }}
                href="http://pzks.fpm.kpi.ua/faculty/entry/magistracy/Prog_IPZ-dod-2020.pdf"
                target="_blank"
              >
                за цим посиланням
              </a>
              .
            </Message.Header>
          </Message>
        </SemanticContainer>
      </Wrapper>
    </Fragment>
  );
};

export default Master;
