import React, { Fragment } from "react";
import Title from "@Components/Title";
import Wrapper from "@Components/Wrapper";
import Breadcrumbs from "@Components/Breadcrumbs";
import { List } from "semantic-ui-react";
import {
  Container as SemanticContainer,
  Message,
  Segment,
} from "semantic-ui-react";

const OfficialDocuments = () => {
  return (
    <Fragment>
      <Wrapper>
        <Breadcrumbs />
        <Title title={"Офіційні документи"} />

        <SemanticContainer textAlign="left" style={{ marginTop: "20px" }}>
          <Segment>
            <List>
              <List.Item
                as="a"
                href="https://pk.kpi.ua/wp-content/uploads/2019/12/rules2020.pdf"
                style={{ marginBottom: "10px" }}
              >
                Правила прийому до КПІ ім. Ігоря Сікорського в 2020 році
              </List.Item>
              <List.Item
                as="a"
                href="https://pk.kpi.ua/wp-content/uploads/2019/12/fdp2020.pdf"
                style={{ marginBottom: "10px" }}
              >
                Положення про випускників системи довузівської підготовки КПІ
                ім. Ігоря Сікорського, які досягли особливих успіхів у навчанні
              </List.Item>
              <List.Item
                as="a"
                href="https://pk.kpi.ua/wp-content/uploads/2019/12/exams2020.pdf"
                style={{ marginBottom: "10px" }}
              >
                Положення про вступні випробування до КПІ ім. Ігоря Сікорського
              </List.Item>
              <List.Item
                as="a"
                href="https://pk.kpi.ua/wp-content/uploads/2019/12/Appeal2020.pdf"
                style={{ marginBottom: "10px" }}
              >
                Положення про порядок подання та розгляду апеляцій вступників до
                КПІ ім. Ігоря Сікорського
              </List.Item>
              <List.Item
                as="a"
                href="https://pk.kpi.ua/wp-content/uploads/2020/02/bl_2020-030220.pdf"
                style={{ marginBottom: "10px" }}
              >
                Інформаційні матеріали приймальної комісії
              </List.Item>
            </List>
          </Segment>

          <Message info>
            <Message.Header>
              Повний перелік офіційних документів знаходиться за{" "}
              <a
                style={{ color: "blue" }}
                href="https://pk.kpi.ua/documents/"
                target="_blank"
              >
                посиланням.
              </a>
            </Message.Header>
          </Message>
        </SemanticContainer>
      </Wrapper>
    </Fragment>
  );
};

export default OfficialDocuments;
