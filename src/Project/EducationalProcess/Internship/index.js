import React from "react";
import Title from "@Components/Title";
import Breadcrumbs from "@Components/Breadcrumbs";
import Wrapper from "@Components/Wrapper";
import Logo from "../../../App/assets/images/img_4718.png";

import {
  Container as SemanticContainer,
  Header,
  Segment,
  List,
  Divider,
  Icon,
  Image,
} from "semantic-ui-react";

const Internship = () => {
  return (
    <Wrapper>
      <Breadcrumbs />
      <Title title={"Практика"} />

      <SemanticContainer textAlign="left">
        <SemanticContainer>
          <Image
            src={Logo}
            size="medium"
            floated="right"
          />
          <Header as="h4">
            Під час навчання студенти кафедри ПЗКС виконують такi види практики:
          </Header>
          <List bulleted>
            <List.Item>
              Бакалаврська підготовка - переддипломна практика – у 8 семестрі
              (квітень-травень)
            </List.Item>
            <List.Item style={{ marginTop: "-160px" }}>
              Магістерська підготовка - науково-дослідна практика – у 12
              семестрі (лютий).
            </List.Item>
          </List>
        </SemanticContainer>

        <Divider />

        <Segment raised color="blue" style={{ marginTop: "20px" }}>
          <Header as="h4">
            З питань проходження практики звертатися до відповідального за
            організацію практики –{" "}
            <a
              style={{ color: "blue" }}
              href="http://intellect.pzks.fpm.kpi.ua/profile/yvy"
              target="_blank"
            >
              доцента Юрчишина Василя Яковича
            </a>{" "}
            (кімн.62, корп. 14) у такі терміни:
          </Header>
          <List>
            <List.Item>
              <Icon name="right triangle" />
              <List.Content>
                <List.Header>
                  переддипломна практика (бакалаври) – у листопад
                </List.Header>
              </List.Content>
            </List.Item>
            <List.Item>
              <Icon name="right triangle" />
              <List.Content>
                <List.Header>науково-дослідна практика – у жовтні.</List.Header>
              </List.Content>
            </List.Item>
          </List>
        </Segment>
      </SemanticContainer>
    </Wrapper>
  );
};

export default Internship;
