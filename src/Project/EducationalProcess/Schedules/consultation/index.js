import React, { Fragment } from "react";
import { Table, Header } from "semantic-ui-react";
import { weekDayTitles } from "../../../../shared/constants/weekDayTitles";

const ConsultationsSchedule = ({ data }) => {
  return (
    <Fragment>

      <Header as="h2" dividing>
        Розклад консультацій
      </Header>
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>№</Table.HeaderCell>
            <Table.HeaderCell>Прізвище та ініціали викладача</Table.HeaderCell>
            <Table.HeaderCell>Дні тижня</Table.HeaderCell>
            <Table.HeaderCell>Час</Table.HeaderCell>
            <Table.HeaderCell>Аудиторія</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data.map((item, index) => {
            const { teacherName, weekDay, timeFrom, timeTo, roomTitle } = item;
            const ordinalNumber = index + 1;

            return (
              <Table.Row>
                <Table.Cell>{ordinalNumber}</Table.Cell>
                <Table.Cell>{teacherName}</Table.Cell>
                <Table.Cell>{weekDayTitles[weekDay]()}</Table.Cell>
                <Table.Cell>{`${timeFrom} - ${timeTo}`}</Table.Cell>
                <Table.Cell>{roomTitle}</Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    </Fragment>
  );
};

export default ConsultationsSchedule;
