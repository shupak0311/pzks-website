const data = [
  {
    studyingYear: 1,
    groupName: "КП-81",
    subject: "АСД",
    room: "311-18",
    teacher: "Погорелов В.В",
    date: "2020-01-10"
  },
  {
    studyingYear: 1,
    groupName: "КП-81",
    subject: "Математичний аналіз",
    room: "93-15",
    teacher: "Легеза В.П.",
    date: "2020-01-15"
  },
  {
    studyingYear: 1,
    groupName: "КП-81",
    subject: "Комп. дискр. математика",
    room: "204-18",
    teacher: "Жабіна В.В.",
    date: "2020-01-19"
  },
  {
    studyingYear: 1,
    groupName: "КП-82",
    subject: "Математичний аналіз",
    room: "93-15",
    teacher: "Легеза В.П.",
    date: "2020-01-11"
  },
  {
    studyingYear: 1,
    groupName: "КП-82",
    subject: "Комп. дискр. математика",
    room: "230-18",
    teacher: "Жабіна В.В.",
    date: "2020-01-15"
  },
  {
    studyingYear: 1,
    groupName: "КП-82",
    subject: "АСД",
    room: "521-19",
    teacher: "Погорелов В.В.",
    date: "2020-01-19"
  },
  {
    studyingYear: 1,
    groupName: "КП-83",
    subject: "Комп. дискр. математика",
    room: "122-16",
    teacher: "Жабіна В.В.",
    date: "2020-01-09"
  },
  {
    studyingYear: 1,
    groupName: "КП-83",
    subject: "АСД",
    room: "502-20",
    teacher: "Погорелов В.В.",
    date: "2020-01-14"
  },
  {
    studyingYear: 1,
    groupName: "КП-83",
    subject: "Математичний аналіз",
    room: "93-15",
    teacher: "Легеза В.П.",
    date: "2020-01-18"
  },
  {
    studyingYear: 2,
    groupName: "КП-71",
    subject: "Математичний аналіз",
    room: "93-15",
    teacher: "Легеза В.П.",
    date: "2020-01-09"
  },
  {
    studyingYear: 2,
    groupName: "КП-72",
    subject: "Математичний аналіз",
    room: "93-15",
    teacher: "Легеза В.П.",
    date: "2020-01-10"
  },
  {
    studyingYear: 2,
    groupName: "КП-73",
    subject: "Математичний аналіз",
    room: "93-15",
    teacher: "Легеза В.П.",
    date: "2020-01-14"
  },
  {
    studyingYear: 2,
    groupName: "КП-71",
    subject: "Теорія ймовірностей",
    room: "3-14",
    teacher: "Сущук-Слюсаренко",
    date: "2020-01-14"
  },
  {
    studyingYear: 2,
    groupName: "КП-72",
    subject: "Теорія ймовірностей",
    room: "3-14",
    teacher: "Сущук-Слюсаренко",
    date: "2020-01-15"
  },
  {
    studyingYear: 2,
    groupName: "КП-73",
    subject: "Теорія ймовірностей",
    room: "3-14",
    teacher: "Сущук-Слюсаренко",
    date: "2020-01-09"
  },
  {
    studyingYear: 2,
    groupName: "КП-71",
    subject: "Фізика",
    room: "104-7",
    teacher: "Іванова І.М.",
    date: "2020-01-19"
  },
  {
    studyingYear: 2,
    groupName: "КП-72",
    subject: "Фізика",
    room: "104-7",
    teacher: "Іванова І.М.",
    date: "2020-01-19"
  },
  {
    studyingYear: 2,
    groupName: "КП-73",
    subject: "Фізика",
    room: "104-7",
    teacher: "Іванова І.М.",
    date: "2020-01-19"
  },
];

export { data };
