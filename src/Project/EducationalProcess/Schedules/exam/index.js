import React, { Fragment } from "react";
import { Table, Header } from "semantic-ui-react";
import Title from "@Components/Title";
import { monthTitles } from "../../../../shared/constants/monthTitles";
import { weekDayTitles } from "../../../../shared/constants/weekDayTitles";

const ExamsSchedule = ({ data }) => {
  const getDateString = (date) => {
    const month = date.getMonth() + 1,
      dayQuantitative = date.getDate(),
      dayOrdinal = date.getDay() === 0 ? 7 : date.getDay();

    const monthTitle = monthTitles[month];
    const dayOrdinalTitle = weekDayTitles[dayOrdinal](true);

    return `${dayQuantitative} ${monthTitle} ${dayOrdinalTitle}`;
  };

  const getSchedule = () => {
    let res = {};

    data.forEach((item) => {
      const { studyingYear, groupName } = item;

      if (!res[studyingYear]) {
        res[studyingYear] = {};
      }

      if (!res[studyingYear][groupName]) {
        res[studyingYear][groupName] = [];
      }

      res[studyingYear][groupName].push(item);
    });

    data.forEach((item) => {
      const { studyingYear, groupName } = item;
      let events = res[studyingYear][groupName];

      events = sortEventsByDate(events);
    });

    return res;
  };

  const sortEventsByDate = (eventsArr) => {
    return eventsArr.sort((a, b) => new Date(a.date) - new Date(b.date));
  };

  const schedule = getSchedule();

  return (
    <Fragment>
      <Header as="h2" dividing>
        Розклад іспитів
      </Header>
      {Object.entries(schedule).map(([studyingYear, groups], index) => {
        return (
          <Fragment key={index}>
            <Title small title={`${studyingYear} курс`} />
            <Table celled>
              <Table.Body>
                {Object.entries(groups).map(([groupName, events], index) => (
                  // Скачай либу для ключей
                  <Fragment key={index * 100}>
                    <Table.Row>
                      <Table.Cell />
                      {events.map(({ date }, index) => (
                        <Table.Cell key={index * 1000}>
                          {getDateString(new Date(date))}
                        </Table.Cell>
                      ))}
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>{groupName}</Table.Cell>
                      {events.map(({ subject, room, teacher }, index) => (
                        <Table.Cell key={index * 10000}>
                          <span>{subject}</span>
                          <br />
                          <span>{room}</span>
                          <br />
                          <span>{teacher}</span>
                        </Table.Cell>
                      ))}
                    </Table.Row>
                  </Fragment>
                ))}
              </Table.Body>
            </Table>
          </Fragment>
        );
      })}
    </Fragment>
  );
};

export default ExamsSchedule;
