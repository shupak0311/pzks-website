import React from "react";
import AnchorLink from "react-anchor-link-smooth-scroll";
import { data as cData } from "./consultation/data";
import { data as eData } from "./exam/data";
import Consultations from "./consultation/index";
import Exams from "./exam/index";
import Breadcrumbs from "@Components/Breadcrumbs";

import Wrapper from "@Components/Wrapper";
import { findPos } from "../../../shared/utils/helpers";

import {
  Container as SemanticContainer,
  Message,
  Header,
  Segment,
  List,
} from "semantic-ui-react";
import { useEffect } from "react";

const SchedulesComponent = ({ path }) => {
  useEffect(() => {
    const elementToScroll = document.getElementById(`${path}`);
    window.scroll({
      top: findPos(elementToScroll),
      behavior: "smooth",
    });
    // elementToScroll.classList.add("focus");

    // setTimeout(() => {
    //   elementToScroll.classList.remove("focus");
    // }, 2000);
  });

  return (
    <Wrapper>
      <Breadcrumbs />

      <SemanticContainer textAlign="center" style={{ marginTop: "20px" }}>
        <List bulleted horizontal>
          <List.Item as="a">
            <AnchorLink href="#tutorial-time">Розклад консультацій</AnchorLink>
          </List.Item>
          <List.Item as="a">
            <AnchorLink href="#examinations">Розклад іспитів</AnchorLink>
          </List.Item>
          <List.Item as="a">
            <AnchorLink href="#timetable">Розклад занять</AnchorLink>
          </List.Item>
          <List.Item as="a">
            <AnchorLink href="#assessment">Терміни атестацій</AnchorLink>
          </List.Item>
        </List>
      </SemanticContainer>
      <SemanticContainer textAlign="left" style={{ marginTop: "10px" }}>
        <SemanticContainer id="tutorial-time">
          <Consultations data={cData} />
        </SemanticContainer>

        <SemanticContainer id="examinations" style={{ marginTop: "30px" }}>
          <Exams data={eData} />
        </SemanticContainer>

        <SemanticContainer id="timetable" style={{ marginTop: "30px" }}>
          <Header as="h2" dividing>
            Розклад занять
          </Header>
          <Message info>
            <Message.Header>
              Розклад занять Ви можете подивитись за цим{" "}
              <a
                style={{ color: "blue" }}
                href="http://rozklad.kpi.ua/Schedules/ScheduleGroupSelection.aspx"
                target="_blank"
              >
                посиланням.
              </a>
            </Message.Header>
          </Message>
        </SemanticContainer>

        <SemanticContainer id="assessment" style={{ marginTop: "30px" }}>
          <Header as="h2" dividing>
            Терміни атестацій
          </Header>
          <Segment style={{ marginTop: "20px" }}>
            <List>
              <List.Item>
                <strong>Перша: 14.10 – 27.10.2019 р.</strong>
              </List.Item>
              <List.Item>
                <strong>Друга: 02.12 – 15.12.2019 р.</strong>
              </List.Item>
            </List>
          </Segment>
        </SemanticContainer>
      </SemanticContainer>
    </Wrapper>
  );
};

export default SchedulesComponent;
