import React, { Fragment, useState } from "react";
import { FlexColumn } from "../../Styles";
import Title from "@Components/Title/index";
import Breadcrumbs from "@Components/Breadcrumbs";
import Wrapper from "@Components/Wrapper";
import { graduationPapersData } from "./graduation.static.data";

import {
  Container as SemanticContainer,
  Header,
  Button,
  Table,
} from "semantic-ui-react";

const GraduationPaper = () => {
  const [activeButtons, setActiveButtons] = useState([]);

  const handleClick = (type, year) => {
    const isOpen = activeButtons.find(
      (x) => x.type === type && x.year === year
    );

    if (isOpen) {
      setActiveButtons(
        activeButtons.filter((x) => x.type !== type || x.year !== year)
      );
    } else {
      setActiveButtons([...activeButtons, { type, year }]);
    }
  };

  const renderBachelors = (bachelors, year) => {
    return (
      <SemanticContainer style={{ marginBottom: "20px" }}>
        <Header as="h3" style={{ marginBottom: "5px" }}>
          Бакалаври
        </Header>
        <Button color="blue" onClick={() => handleClick("bachelors", year)}>
          {activeButtons.find((x) => x.type === "bachelors" && x.year === year)
            ? "Згорнути"
            : "Розгорнути"}
        </Button>
        {activeButtons.find(
          (x) => x.type === "bachelors" && x.year === year
        ) && (
          <Table celled>
            <Table.Header fullWidth>
              <Table.Row>
                <Table.HeaderCell>№</Table.HeaderCell>
                <Table.HeaderCell>Група</Table.HeaderCell>
                <Table.HeaderCell>П.І.Б</Table.HeaderCell>
                <Table.HeaderCell>Тема</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            {bachelors.map((item, index) => (
              <Table.Body>
                <Table.Row>
                  <Table.Cell>{index + 1}</Table.Cell>
                  <Table.Cell>{item.groupUA}</Table.Cell>
                  <Table.Cell>{item.nameUA}</Table.Cell>
                  <Table.Cell>
                    <FlexColumn>
                      <strong>{item.themeUA}</strong>
                      <Button
                        style={{ marginTop: "8px" }}
                        color="blue"
                        as="a"
                        href={item.link}
                        target="_blank"
                      >
                        Завантажити текстову частину
                      </Button>
                    </FlexColumn>
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            ))}
          </Table>
        )}
      </SemanticContainer>
    );
  };

  const renderSpecialists = (specialists, year) => {
    return (
      <SemanticContainer style={{ marginBottom: "20px" }}>
        <Header as="h3" style={{ marginBottom: "5px" }}>
          Спеціалісти
        </Header>
        <Button color="blue" onClick={() => handleClick("specialists", year)}>
          {activeButtons.find(
            (x) => x.type === "specialists" && x.year === year
          )
            ? "Згорнути"
            : "Розгорнути"}
        </Button>
        {activeButtons.find(
          (x) => x.type === "specialists" && x.year === year
        ) && (
          <Table celled>
            <Table.Header fullWidth>
              <Table.Row>
                <Table.HeaderCell>№</Table.HeaderCell>
                <Table.HeaderCell>Група</Table.HeaderCell>
                <Table.HeaderCell>П.І.Б</Table.HeaderCell>
                <Table.HeaderCell>Тема</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            {specialists.map((item, index) => (
              <Table.Body>
                <Table.Row>
                  <Table.Cell>{index + 1}</Table.Cell>
                  <Table.Cell>{item.groupUA}</Table.Cell>
                  <Table.Cell>{item.nameUA}</Table.Cell>
                  <Table.Cell>
                    <FlexColumn>
                      <strong>{item.themeUA}</strong>
                      <Button
                        style={{ marginTop: "8px" }}
                        color="blue"
                        as="a"
                        href={item.link}
                        target="_blank"
                      >
                        Завантажити текстову частину
                      </Button>
                    </FlexColumn>
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            ))}
          </Table>
        )}
      </SemanticContainer>
    );
  };

  const renderMasters = (masters, year) => {
    return (
      <SemanticContainer>
        <Header as="h3" style={{ marginBottom: "5px" }}>
          Магістри
        </Header>
        <Button color="blue" onClick={() => handleClick("masters", year)}>
          {activeButtons.find((x) => x.type === "masters" && x.year === year)
            ? "Згорнути"
            : "Розгорнути"}
        </Button>
        {activeButtons.find((x) => x.type === "masters" && x.year === year) && (
          <Table celled>
            <Table.Header fullWidth>
              <Table.Row>
                <Table.HeaderCell>№</Table.HeaderCell>
                <Table.HeaderCell>Група</Table.HeaderCell>
                <Table.HeaderCell>П.І.Б</Table.HeaderCell>
                <Table.HeaderCell>Тема</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            {masters.map((item, index) => (
              <Table.Body>
                <Table.Row>
                  <Table.Cell>{index + 1}</Table.Cell>
                  <Table.Cell>{item.groupUA}</Table.Cell>
                  <Table.Cell>{item.nameUA}</Table.Cell>
                  <Table.Cell>
                    <FlexColumn>
                      <strong>{item.themeUA}</strong>
                      <Button
                        style={{ marginTop: "8px" }}
                        color="blue"
                        as="a"
                        href={item.link}
                        target="_blank"
                      >
                        Завантажити текстову частину
                      </Button>
                    </FlexColumn>
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            ))}
          </Table>
        )}
      </SemanticContainer>
    );
  };

  return (
    <Wrapper>
      <Breadcrumbs />
      <Title title={"Випускні кваліфікаційні роботи"} />

      <SemanticContainer textAlign="left" style={{ marginTop: "20px" }}>
        {graduationPapersData.map(
          ({ year, bachelors, specialists, masters }) => {
            return (
              <Fragment>
                <Header as="h3" dividing textAlign="center">
                  {year} рік
                </Header>
                {renderBachelors(bachelors, year)}
                {renderSpecialists(specialists, year)}
                {renderMasters(masters, year)}
              </Fragment>
            );
          }
        )}
      </SemanticContainer>
    </Wrapper>
  );
};

export default GraduationPaper;
