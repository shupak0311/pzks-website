export const graduationPapersData = [
  {
    year: "2017-2018",
    bachelors: [
      {
        groupEN: "KP-41",
        nameEN: "Dmytro Bohutskyi",
        themeEN:
          "Web resource for organization of teaching and professional development",
        groupUA: "КП-41",
        nameUA: "Богуцький Дмитро Борисович",
        themeUA: "Веб-ресурс для організації навчання та професійного розвитку",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Bohutskyi_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Olha Vitiaz",
        themeEN:
          "System of software generation of works of art. Generation module",
        groupUA: "КП-41",
        nameUA: "Вітязь Ольга Володимирівна",
        themeUA:
          "Система програмної генерації витворів мистецтва. Модуль генерації",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Vitiaz_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Eugenia Glinska",
        themeEN:
          "Software tool for client of catering facilities network service",
        groupUA: "КП-41",
        nameUA: "Глінська Євгенія Михайлівна",
        themeUA:
          "Програмне забезпечення для обслуговування клієнтів мережі закладів громадського харчування",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Glinska_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Vladislav Glinsky",
        themeEN: "Multimedia data processing language compiler",
        groupUA: "КП-41",
        nameUA: "Глінський Владислав Васильович",
        themeUA: "Компілятор мови обробки мультимедійних даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Glinsky_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Vlada Yerastova",
        themeEN: "Software model for forecasting market price of software",
        groupUA: "КП-41",
        nameUA: "Єрастова Влада Юріївна",
        themeUA:
          "Програмна модель прогнозування ринкової вартості програмного забезпечення",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Yerastova_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Volodymyr Korzun",
        themeEN:
          "Programming tools for landmark recognition and landmark retrieval",
        groupUA: "КП-41",
        nameUA: "Корзун Володимир Ігорович",
        themeUA:
          "Програмні засоби розпізнавання об'єктів місцевості на зображеннях",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Korzun_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Taras Lavreniuk",
        themeEN:
          "Cloud service for structuring and storing person physical activity data",
        groupUA: "КП-41",
        nameUA: "Лавренюк Тарас Олександрович",
        themeUA:
          "Хмарний сервіс для структурування та збереження даних фізичного навантаження користувача",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Lavreniuk_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Olexandr Lysenko",
        themeEN: "Web-resourse «Electronic school library»",
        groupUA: "КП-41",
        nameUA: "Лисенко Олександр Олегович",
        themeUA: "Веб-ресурс «Електронна шкільна бібліотека»",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Lysenko_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Ihor Los",
        themeEN: "Software for procedural texture generation. Shader library",
        groupUA: "КП-41",
        nameUA: "Лось Ігор Анатолійович",
        themeUA:
          "Програмне забезпечення для процедурної генерації текстур. Бібліотека шейдерів",
        link: "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Los_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Valeriia Nedilska",
        themeEN: "Software for shipping company vehicle tracking support",
        groupUA: "КП-41",
        nameUA: "Недільська Валерія Дмитрівна",
        themeUA:
          "Програмне забезпечення для підтримки трекінгу транспортних засобів компанії-перевізника",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Nedilska_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Bogdan Oros",
        themeEN: "Web resource for organization of volunteer computations",
        groupUA: "КП-41",
        nameUA: "Орос Богдан Богданович",
        themeUA: "Веб-ресурс для організації волонтерських обчислень",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Oros_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Kyrylo Redin",
        themeEN:
          "Social web application for thematic storage and sharing educational multimedia information",
        groupUA: "КП-41",
        nameUA: "Редін Кирило Артурович",
        themeUA:
          "Соціальний веб-додаток для тематичного збереження та розповсюдження навчальної мультимедійної інформації",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Redin_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Kostiantyn Rudenko",
        themeEN:
          "Software tool for procedural texture generation. Application core",
        groupUA: "КП-41",
        nameUA: "Руденко Костянтин Павлович",
        themeUA:
          "Програмне забезпечення для процедурної генерації текстур. Ядро програми",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Rudenko_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Amir Sanhinov",
        themeEN:
          "JavaScript library for solving navier-stokes equations with hardware acceleration",
        groupUA: "КП-41",
        nameUA: "Сангінов Амір Ільхомджонович",
        themeUA:
          "Бібліотека мовою JavaScript для розв’язання рівнянь Нав’є-Стокса з використанням апаратного прискорення",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Sanhinov_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Taras Sakharchuk",
        themeEN:
          "System for collecting and analyzing user feedback for educational process",
        groupUA: "КП-41",
        nameUA: "Сахарчук Тарас Юрійович",
        themeUA:
          "Система збору та аналізу відгуків користувачів освітнього процесу",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Sakharchuk_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Andrii Severin",
        themeEN:
          "Methods and algorithms for performing computation over the elements of GF(pm) in polynomial and normal bases",
        groupUA: "КП-41",
        nameUA: "Северін Андрій Іванович",
        themeUA:
          "Методи та алгоритми виконання обчислень над елементами поля GF(pm) у поліноміальному та нормальному базисах",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Severin_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Roman Stylyk",
        themeEN:
          "Side-effects analysis subsystem for javascript static code analyzer",
        groupUA: "КП-41",
        nameUA: "Стилик Роман Григорович",
        themeUA:
          "Підсистема аналізу side-ефектів статичного аналізатора коду для мови JavaScript",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Stylyk_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Borys Topchiiev",
        themeEN:
          "System of software generation of works of art. Generation module",
        groupUA: "КП-41",
        nameUA: "Топчієв Борис Сергійович",
        themeUA:
          "Система програмної генерації витворів мистецтва. Модуль обробки даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Topchiiev_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Karim Khuder",
        themeEN:
          "Mobile application for keeping an information diary of people with diabetes mellitus. Data collection and processing module",
        groupUA: "КП-41",
        nameUA: "Худер Карім Нідаль",
        themeUA:
          "Мобільний додаток для ведення інформаційного щоденника хворими на цукровий діабет. Модуль збору та обробки даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Khuder_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Maksym Tsarikov",
        themeEN:
          "Application for monitoring and consulting of users of commercial online-services",
        groupUA: "КП-41",
        nameUA: "Царіков Максим Сергійович",
        themeUA:
          "Додаток для моніторингу та консультування користувачів комерційних онлайн-сервісів",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Tsarikov_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Roman Shabinevych",
        themeEN:
          "Web-resource for collection and analysing of statistical data about book funds of general educational institutions",
        groupUA: "КП-41",
        nameUA: "Шабіневич Роман Олександрович",
        themeUA:
          "Веб-ресурс для збору та аналізу статистичних даних про книжкові фонди загальноосвітніх навчальних закладів",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Shabinevych_KP_41.pdf",
      },
      {
        groupEN: "KP-41",
        nameEN: "Mariia Shyn",
        themeEN: "Software application for forecasting tourism activity",
        groupUA: "КП-41",
        nameUA: "Шин Марія Петрівна",
        themeUA: "Програмний додаток прогнозування туристичної активності",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Shyn_KP_41.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Andrii Abramov",
        themeEN: "Real-time trnsaction security ensurance subsystem",
        groupUA: "КП-42",
        nameUA: "Абрамов Андрій Володимирович",
        themeUA:
          "Підсистема забезпечення безпеки транзакцій в режимі реального часу",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Abramov_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Daria Bilohub",
        themeEN:
          "The software for modeling of optimum placement of recreational facilities",
        groupUA: "КП-42",
        nameUA: "Білогуб Дар’я Сергіївна",
        themeUA:
          "Програмне забезпечення для моделювання оптимального розміщення закладів відпочинку",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Bilohub_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Alexander Boliachyi",
        themeEN:
          "Application for converting raster images to vector with usage of neural networks",
        groupUA: "КП-42",
        nameUA: "Болячий Олександр Андрійович",
        themeUA:
          "Програмний засіб для конвертації растрового зображення у векторне за допомогою нейронних мереж",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Boliachyi_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Erik Himiranov",
        themeEN: "System for monitoring the operation of a web resource",
        groupUA: "КП-42",
        nameUA: "Гіміранов Ерік Ільдарович",
        themeUA: "Система моніторингу роботи веб-ресурсу",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Himiranov_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Oleksandr Kozhokar",
        themeEN:
          "Software for a device for the automatic calculation and transfer of user physical load data",
        groupUA: "КП-42",
        nameUA: "Кожокарь Олександр Романович",
        themeUA:
          "Програмне забезпечення для пристрою з автоматизованого підрахунку та передачі даних фізичного навантаження користувача",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Kozhokar_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Anton Lebodkin",
        themeEN: "Software for videocalls with 3D visualization",
        groupUA: "КП-42",
        nameUA: "Лебьодкін Антон Миколайович",
        themeUA:
          "Програмне забезпечення для відео-зв'язку з тривимірною візуалізацією",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Lebodkin_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Dmytro Lysohor",
        themeEN: "Multimedia data processing language compiler",
        groupUA: "КП-42",
        nameUA: "Лисогор Дмитро Юрійович",
        themeUA: "Програмні засоби для підтримки рефакторингу вихідного коду",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Lysohor_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Oleksandr Melashchenko",
        themeEN:
          "Service for analysis of web resources rating in search engines",
        groupUA: "КП-42",
        nameUA: "Мелащенко Олександр Сергійович",
        themeUA:
          "Програмний сервіс для аналізу рейтингу веб-ресурсів у пошукових системах",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Melashchenko_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Mykyta Mykhailenko",
        themeEN:
          "Automated system of selection of IT personnel for recreational companies",
        groupUA: "КП-42",
        nameUA: "Михайленко Микита Михайлович",
        themeUA:
          "Автоматизована система підбору IT-персоналу для рекрутингових компаній",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Mykhailenko_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Serhii Prykhodko",
        themeEN: "Software for sentiment analysis of customer reviews",
        groupUA: "КП-42",
        nameUA: "Приходько Сергій Олександрович",
        themeUA:
          "Програмне забезпечення сентимент-аналізу відгуків користувачів",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Prykhodko_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Maksym Ratoshniuk",
        themeEN:
          "Decentralized platform of automation the process of the management by the educational processes of secondary education in Ukraine",
        groupUA: "КП-42",
        nameUA: "Ратошнюк Максим Юрійович",
        themeUA:
          "Децентралізована платформа для автоматизації процесу керування навчальним процесом закладів середньої освіти в Україні",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Ratoshniuk_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Vira Rodionova",
        themeEN:
          "Mobile application for keeping an information diary of people with diabetes mellitus. Communication module",
        groupUA: "КП-42",
        nameUA: "Родіонова Віра Олексіївна",
        themeUA:
          "Мобільний додаток для ведення інформаційного щоденника хворими на цукровий діабет. Комунікаційний модуль",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Rodionova_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Suleimanov Ilias",
        themeEN:
          "Software of accounting goods on the basis of documentary report",
        groupUA: "КП-42",
        nameUA: "Сулейманов Ільяс Ібрагімович",
        themeUA: "Програмні засоби обліку товарів на основі документного звіту",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Suleimanov_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Andriy Sushchyk",
        themeEN:
          "An information system for the organization of educational process in secondary educational institutions",
        groupUA: "КП-42",
        nameUA: "Сущик Андрій Миколайович",
        themeUA:
          "Інформаційна система організації навчального процесу загальноосвітніх закладів",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Sushchyk_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Mykhailo Khmelovskyi",
        themeEN:
          "Software system for support of the activity of the legal department of the banking institution",
        groupUA: "КП-42",
        nameUA: "Хмеловський Михайло Євгенійович",
        themeUA:
          "Програмна система для підтримки діяльності юридичного відділу банківської установи",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Khmelovsky_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Chepurnyi Oleksandr",
        themeEN: "Software for automated classification of e-commerce products",
        groupUA: "КП-42",
        nameUA: "Чепурний Олександр Вiталійович",
        themeUA:
          "Програмні засоби автоматичної класифікації в галузі електронної комерції",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Chepurnyi_KP_42.pdf",
      },
      {
        groupEN: "KP-42",
        nameEN: "Bohdan Yatsuk",
        themeEN:
          "Mobile application for monitoring and travel planning in route city taxis",
        groupUA: "КП-42",
        nameUA: "Яцук Богдан Олександрович",
        themeUA:
          "Мобільний додаток для моніторингу та планування поїздок у маршрутних таксі міста",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/bachelor/Yatsuk_KP_42.pdf",
      },
    ],
    specialists: [
      {
        groupEN: "KP-61с",
        nameEN: "Bohachevskyi Dmytro",
        themeEN:
          "Mobile application distributed system control and monitoring traffic",
        groupUA: "КП-61с",
        nameUA: "Богачевський Дмитро Іванович",
        themeUA:
          "Мобільний додаток розподіленої системи контролю і моніторингу дорожнього руху",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/specialists/Bohachevskyi_KP_61c.pdf",
      },
      {
        groupEN: "KP-61с",
        nameEN: "Vladyslav Zaichuk",
        themeEN:
          "Distributed program system for ownership management based on blockchain technology",
        groupUA: "КП-61с",
        nameUA: "Зайчук Владислав Віталійович",
        themeUA:
          "Розподілена програмна система керування майновими правами на основі блокчейн технології",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/specialists/Zaichuk_KP_61c.pdf",
      },
      {
        groupEN: "KP-61с",
        nameEN: "Anhelina Lebedieva",
        themeEN:
          "Software complex of annonymality payment for electronic payments in banking systems",
        groupUA: "КП-61с",
        nameUA: "Лебедєва Ангеліна Олександрівна",
        themeUA:
          "Програмний комплекс забезпечення анонімності проведення електронних платежів у банківських системах",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/specialists/Lebedieva_KP_61c.pdf",
      },
      {
        groupEN: "KP-61с",
        nameEN: "Vadym Lytvyn",
        themeEN:
          "Software for introduction of document management system and management in hospitals. Mobile application",
        groupUA: "КП-61с",
        nameUA: "Литвин Вадим Олександрович",
        themeUA:
          "Програмне забезпечення для впровадження електронного документообігу та управління в медичних закладах. Мобільний додаток",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/specialists/Lytvyn_KP_61c.pdf",
      },
      {
        groupEN: "KP-61с",
        nameEN: "Mykola Ohoiko",
        themeEN:
          "Software for source code generation of basic functions three-tier web-applications",
        groupUA: "КП-61с",
        nameUA: "Огойко Микола Васильович",
        themeUA:
          "Програмні засоби для генерації вихідного коду базових функцій трирівневих веб-додатків",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/specialists/Ohoiko_KP_61c.pdf",
      },
    ],
    masters: [
      {
        groupEN: "KP-61m",
        nameEN: "Andrii Bartkoviak",
        themeEN:
          "Method of automated classification of short news texts with the use of named entities",
        groupUA: "КП-61м",
        nameUA: "Бартков’як Андрій Юліанович",
        themeUA:
          "Метод автоматизованої класифікації коротких новинних текстів з використанням іменованих сутностей",
        link: "http://pzks.fpm.kpi.ua/diploma/2017-2018/master/Bartkoviak.pdf",
      },
      {
        groupEN: "KP-61m",
        nameEN: "Vladyslav Hurov",
        themeEN:
          "Model of realization of coarse-grained parallelism in multiprocessor systems",
        groupUA: "КП-61м",
        nameUA: "Гуров Владислав Олександрович",
        themeUA:
          "Модель реалізації крупнозернистого паралелізму в мультипроцесорних системах",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/masters/Hurov_KP-61m.pdf",
      },
      {
        groupEN: "KP-61m",
        nameEN: "Andrii Dychka",
        themeEN:
          "Modification of the method of multiple scalar multiplication of points of the elitptic curve in finite fields",
        groupUA: "КП-61м",
        nameUA: "Дичка Андрій Іванович",
        themeUA:
          "Модифікований метод багатократного скалярного множення точок еліптичної кривої у скінченних полях",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/masters/Dychka_KP-61m.pdf",
      },
      {
        groupEN: "KP-61m",
        nameEN: "Artem Kapura",
        themeEN:
          "Model of data flow system for execution multilateral operations",
        groupUA: "КП-61м",
        nameUA: "Капура Артем Сергійович",
        themeUA: "Модель потокової системи для виконання багатомісних операцій",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/masters/Kapura_KP-61m.pdf",
      },
      {
        groupEN: "KP-61m",
        nameEN: "Anna Sokolovska",
        themeEN:
          "Method of automated detection of abusive content in social media text messages",
        groupUA: "КП-61м",
        nameUA: "Соколовська Анна Віталіївна",
        themeUA:
          "Метод автоматизованого визначення образливого вмісту текстових повідомлень в соціальних мережах",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/masters/Sokolovska_KP-61m.pdf",
      },
      {
        groupEN: "KP-61m",
        nameEN: "Felindash Valeria",
        themeEN:
          "Method of analytical processing of big data for the service of analytics of international trading data",
        groupUA: "КП-61м",
        nameUA: "Феліндаш Валерія Віталіївна",
        themeUA:
          "Метод аналітичної обробки великих даних для сервісу аналітичних досліджень міжнародних торгівельних даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/masters/Felindash_KP-61m.pdf",
      },
      {
        groupEN: "KP-61m",
        nameEN: "Denys Chernykh",
        themeEN: "Rasterization method for 3D medical image cutting",
        groupUA: "КП-61м",
        nameUA: "Черних Денис Андрійович",
        themeUA: "Метод растеризації зрізів тривимірних медичних зображень",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/masters/Chernykh_KP-61m.pdf",
      },
      {
        groupEN: "KP-61m",
        nameEN: "Mykhailo Shevchuk",
        themeEN:
          "Modified method of automated identity comparison of text data which uses data compression",
        groupUA: "КП-61м",
        nameUA: "Шевчук Михайло Михайлович",
        themeUA:
          "Модифікований метод автоматизованого порівняння текстів на ідентичність з використанням ущільнення даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/masters/Shevchuk_KP-61m.pdf",
      },
      {
        groupEN: "KP-61m",
        nameEN: "Yakiv Yusyn",
        themeEN:
          "Modified method of island clustering of natural language text data",
        groupUA: "КП-61м",
        nameUA: "Юсин Яків Олексійович",
        themeUA:
          "Модифікований метод острівної кластеризації природномовних текстових даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2017-2018/masters/Yusyn_KP-61m.pdf",
      },
    ],
  },
  {
    year: "2016-2017",
    bachelors: [
      {
        groupEN: "KP-31",
        nameEN: "Dmytro Blazhevskyi",
        themeEN:
          "Mobile application to support diabetes treatment. Communication module",
        groupUA: "КП-31",
        nameUA: "Блажевський Дмитро Ростиславович",
        themeUA:
          "Мобільний додаток для підтримки лікування хворих на діабет. Комунікаційний модуль",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Blazhevskyi_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Vladislav Hlinskyi",
        themeEN: "Video search system of wanted vehicles",
        groupUA: "КП-31",
        nameUA: "Глінський Владислав Володимирович",
        themeUA:
          "Система відео-пошуку транспортних засобів, що перебувають у розшуку",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Hlinskiy_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Vladyslav Hordiienko",
        themeEN:
          "Software for processing three-dimensional object with a stereo camera",
        groupUA: "КП-31",
        nameUA: "Гордієнко Владислав Ігорович",
        themeUA:
          "Програмне забезпечення для обробки тривимірних об’єктів за допомогою стереокамери",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Hordiienko_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Oleksii Zamekula",
        themeEN:
          "Software tools for orginizationand analysis results of online surveys",
        groupUA: "КП-31",
        nameUA: "Замекула Олексій Ігорович",
        themeUA:
          "Програмні засоби для організації та аналізу результатів онлайн-опитувань",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Zamekula_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Ivan Kolomiiets",
        themeEN:
          "Software to manage «smart home» with technology kinect. Core system",
        groupUA: "КП-31",
        nameUA: "Коломієць Іван Валерійович",
        themeUA:
          "Програмне забезпечення для керування «розумним будинком» за допомогою технології Kinect. Ядро системи",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Kolomiiets_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Dmytro Landiak",
        themeEN:
          "Software for prediction passenger traffic and organization transport enterprize. The server side",
        groupUA: "КП-31",
        nameUA: "Ландяк Дмитро Петрович",
        themeUA:
          "Програмне забезпечення для прогнозування пасажиропотоку та організації рухомого складу АТП. Серверна частина",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Landiak_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Daryna Lapchuk",
        themeEN:
          "Information system for urban bike trails data collection and analysis",
        groupUA: "КП-31",
        nameUA: "Лапчук Дарина Анатоліївна",
        themeUA:
          "Android-додаток для збору та аналізу інформації про міські веломаршрути",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Lapchuk_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Ivan Liubashenko",
        themeEN:
          "Interactive system for searching, filtering and processing files",
        groupUA: "КП-31",
        nameUA: "Любашенко Іван Дмитрович",
        themeUA:
          "Інтерактивна система для пошуку, фільтрації та обробки файлів",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Liubashenko_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Mykolaichyk Volodymir",
        themeEN:
          "Software for prediction passenger traffic and organization transport enterprize. The client side",
        groupUA: "КП-31",
        nameUA: "Миколайчик Володимир Вікторович",
        themeUA:
          "Програмне забезпечення для прогнозування пасажиропотоку та організації рухомого складу АТП. Клієнтська частина",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Mykolaichyk_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Yurii Mukhin",
        themeEN: "Software for gesture recognition via web-cam",
        groupUA: "КП-31",
        nameUA: "Мухін Юрій Юрійович",
        themeUA:
          "Програмне забезпечення для розпізнавання мови жестів за допомогою веб-камери",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Mukhin_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Serhii Nedilko",
        themeEN:
          "Software to manage «smart home» with technology kinect. Kinect module",
        groupUA: "КП-31",
        nameUA: "Неділько Сергій Вікторович",
        themeUA:
          "Програмне забезпечення для керування «розумним будинком» за допомогою технології Kinect. Модуль роботи з контролером Kinect",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Nedilko_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Vladyslav Petrus",
        themeEN:
          "Mobile application to support diabetes treatment. Data collection and processing module",
        groupUA: "КП-31",
        nameUA: "Петрусь Владислав Ігорович",
        themeUA:
          "Мобільний додаток для підтримки лікування хворих на діабет. Модуль збору та обробки даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Petrus_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Roman Pylypenko",
        themeEN: "Web application for instant messaging. Server part",
        groupUA: "КП-31",
        nameUA: "Пилипенко Роман Олександрович",
        themeUA:
          "Веб-додаток для миттєвого обміну повідомленнями. Серверна частина",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Pylypenko_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Romanova Kseniia",
        themeEN:
          "Mobile application for accounting and analytical research user`s current tasks",
        groupUA: "КП-31",
        nameUA: "Романова Ксенія Євгенівна",
        themeUA:
          "Мобільний додаток для обліку та аналітичного дослідження поточних задач користувача",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Romanova_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Viktoriia Romanchenko",
        themeEN:
          "Software tools for full-text search in non-relational databases",
        groupUA: "КП-31",
        nameUA: "Романченко Вікторія Вікторівна",
        themeUA:
          "Програмні засоби повнотекстового пошуку у нереляційних базах даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Romanchenko_KP_51.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Andrew Stupnitsky",
        themeEN: "Software for information support for patients of hospital",
        groupUA: "КП-31",
        nameUA: "Ступницький Андрій Сергійович",
        themeUA:
          "Програмне забезпечення для інформаційної підтримки пацієнтів лікарні",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Stupnitsky_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Oleksandr Khivrych",
        themeEN:
          "Software for static code analysis of source code based on quality metrics",
        groupUA: "КП-31",
        nameUA: "Хіврич Олександр Ігорович",
        themeUA:
          "Програмне забезпечення для статичного аналізу вихідного коду на основі метрик якості",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Khivrych_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Oksana Chorna",
        themeEN:
          "Automated system for queue management and accounting workflow of human resources",
        groupUA: "КП-31",
        nameUA: "Чорна Оксана Вікторівна",
        themeUA:
          "Автоматизована система управління чергою та обліку завантаження людських ресурсів виробництва",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Chorna_KP_31.pdf",
      },
      {
        groupEN: "KP-31",
        nameEN: "Yuliia Shpak",
        themeEN: "Web application for instant messaging. Client part",
        groupUA: "КП-31",
        nameUA: "Шпак Юлія Сергіївна",
        themeUA:
          "Веб-додаток для миттєвого обміну повідомленнями. Клієнтська частина",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Shpak_KP_31.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Dmytro Aharkov",
        themeEN:
          "Method of executing operations over elements of field GF(p^m)",
        groupUA: "КП-32",
        nameUA: "Агарков Дмитро Олександрович",
        themeUA: "Метод виконання операцій над елементами поля GF(p^m)",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Aharkov_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Yevhenii Vasin",
        themeEN:
          "Software component of the intrusion detection system based on the technology of sensory traps",
        groupUA: "КП-32",
        nameUA: "Васін Євгеній Васильович",
        themeUA:
          "Програмний компонент системи виявлення вторгнень на основі технології сенсорних пасток",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Vasin_Yevhenii_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Konstantin Vasin",
        themeEN:
          "Communication module of construction software system of production bines-process for small and middle business",
        groupUA: "КП-32",
        nameUA: "Васін Костянтин Васильович",
        themeUA:
          "Комунікаційний модуль програмної системи конструювання бізнес- процесів виробництва для малого та середнього бізнесу",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Vasin_Kostiantyn_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Denys Vinnyk",
        themeEN: "Software for synthesis images in the infrared spectrum",
        groupUA: "КП-32",
        nameUA: "Вінник Денис Андрійович",
        themeUA:
          "Програмне забезпечення для синтезу зображень в інфрачервоному спектрі",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Vinnyk_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Kateryna Hurenko",
        themeEN: "Software fot processing large volumes of sensory data",
        groupUA: "КП-32",
        nameUA: "Гуренко Катерина Анатоліївна",
        themeUA: "Програмні засоби обробки великих обсягів сенсорних даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Hurenko_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Dzizinska Valeriia",
        themeEN:
          "Automated library information system for schools. Cataloging module",
        groupUA: "КП-32",
        nameUA: "Дзізінська Валерія Віталіївна",
        themeUA:
          "Автоматизована бібліотечна інформаційна система для загальноосвітніх шкіл. Модуль каталогізації фонду",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Dzizinska_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Yevhen Dorodnikov",
        themeEN:
          "Web-resource “Electronic database for Sikorsky Challenge projects”",
        groupUA: "КП-32",
        nameUA: "Дородніков Євген Сергійович",
        themeUA: "Веб-ресурс “Електронна база проектів Sikorsky Challenge”",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Dorodnikov_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Oleksandr Drobot",
        themeEN:
          "Software for managing virtual objects using the kinect controller and virtual reality gloves",
        groupUA: "КП-32",
        nameUA: "Дробот Олександр Олександрович",
        themeUA:
          "Програмне забезпечення для управління віртуальними об’єктами за допомогою контролера Kinect та рукавички віртуальної реальності",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Drobot_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Yeryhin Maksym",
        themeEN: "Software for visualizing panoramic images on mobile devices",
        groupUA: "КП-32",
        nameUA: "Єригін Максим Павлович",
        themeUA:
          "Програмне забезпечення для візуалізації панорамних зображень на мобільних пристроях",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Yeryhin_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Yuri Zhykin",
        themeEN:
          "User notification service for OpenBazaar peer-to-peer E-commerce network",
        groupUA: "КП-32",
        nameUA: "Жикін Юрій Сергійович",
        themeUA:
          "Сервіс користувацьких оповіщень для однорангової мережі електронної торгівлі OpenBazaar",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Zhykin_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Serhii Kopach",
        themeEN: "Online queue management software",
        groupUA: "КП-32",
        nameUA: "Копач Сергій Миколайович",
        themeUA: "Програмні засоби для ведення онлайн-черги",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Kopach_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Yurii Lampiha",
        themeEN: "Software library for WebRTC technology research",
        groupUA: "КП-32",
        nameUA: "Лампіга Юрій Анатолійович",
        themeUA: "Бібліотека програм для дослідження технології WebRTC",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Lampiha_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Borys Lukashov",
        themeEN: "Yield forecasting system for winter crop",
        groupUA: "КП-32",
        nameUA: "Лукашов Борис Олегович",
        themeUA:
          "Cистема прогнозування врожайності сільськогосподарських озимих",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Lukashov_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Inokentiy Mazhara",
        themeEN:
          "Communication module of automated knowledge management system with usage of mnemotechnologies",
        groupUA: "КП-32",
        nameUA: "Мажара Інокентій Віталійович",
        themeUA:
          "Комунікаційний модуль автоматизованої системи управління знаннями з використанням мнемотехнологій",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Mazhara_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Yevhenii Maliavka",
        themeEN:
          "Software tools for automated buildling of documentation for React-UI components",
        groupUA: "КП-32",
        nameUA: "Малявка Євгеній Анатолійович",
        themeUA:
          "Програмні засоби для автоматичної побудови документації до React-компонентів інтерфейсу користувача",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Maliavka_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Artur Nazarov",
        themeEN:
          "Driver eXist-db database for language interpreter Common Lisp",
        groupUA: "КП-32",
        nameUA: "Назаров Артур Равшанович",
        themeUA:
          "Драйвер бази даних eXist-db для інтерпретатора мови Common Lisp",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Nazarov_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Riabchenko Dmytro",
        themeEN:
          "Web application for translation of english Technical documents",
        groupUA: "КП-32",
        nameUA: "Рябченко Дмитро Олександрович",
        themeUA: "Веб-додаток для перекладу англомовної технічної документації",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Riabchenko_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Shevchuk Evgeniy",
        themeEN:
          "Analytical module software system design businessprocesses for the production of small and medium enterprises",
        groupUA: "КП-32",
        nameUA: "Шевчук Євгеній Олегович",
        themeUA:
          "Програмно-аналітичний модуль системи конструювання бізнес-процесів виробництва для малого та середнього бізнесу",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Shevchuk_KP_32.pdf",
      },
      {
        groupEN: "KP-32",
        nameEN: "Yakhin Serhii",
        themeEN:
          "Automated library information system for schools. Circulation of books module",
        groupUA: "КП-32",
        nameUA: "Яхін Сергій Леонідович",
        themeUA:
          "Автоматизована бібліотечна інформаційна система для загальноосвітніх шкіл. Модуль обігу книг",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/bachelor/Yakhin_KP_32.pdf",
      },
    ],
    specialists: [
      {
        groupEN: "KP-51c",
        nameEN: "Viktor Danyliuk",
        themeEN:
          "System For Analysis, Storage And Search Information For Automated Knowledge Management System Using Mnemotehnologies",
        groupUA: "КП-51с",
        nameUA: "Данилюк Віктор Вікторович",
        themeUA:
          "Система аналізу, збереження та пошуку інформації для автоматизованої системи управління знаннями з використанням мнемотехнологій",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/specialists/Danyliuk_KP_51c.pdf",
      },
      {
        groupEN: "KP-51c",
        nameEN: "Mariia Kozak",
        themeEN: "Information Computer System Kamenez-Podolsk Art Museum",
        groupUA: "КП-51с",
        nameUA: "Козак Марія Сергіївна",
        themeUA:
          "Інформаційно-комп’ютерна система Кам’янець-Подільського художнього музею",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/specialists/Kozak_KP_51c.pdf",
      },
      {
        groupEN: "KP-51c",
        nameEN: "Kyshnarova Anastasiia",
        themeEN: "Software Financial Analytics Of Business Planning",
        groupUA: "КП-51с",
        nameUA: "Кушнарьова Анастасія Вячеславівна",
        themeUA:
          "Програмне забезпечення фінансово-аналітичної системи бізнес-планування",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/specialists/Kyshnarova_KP_51c.pdf",
      },
      {
        groupEN: "KP-51c",
        nameEN: "Priadko Dmytro",
        themeEN:
          "Intelligent Interface For Creation Of Individual Knowledge Bases",
        groupUA: "КП-51с",
        nameUA: "Прядко Дмитро Юрійович",
        themeUA:
          "Інтелектуальний інтерфейс для створення індивідуальних баз знань",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/specialists/Priadko_KP_51c.pdf",
      },
    ],
    masters: [
      {
        groupEN: "KP-51m",
        nameEN: "Yevhenii Kyselov",
        themeEN:
          "Analysis method of software continuous integration strategy with microservices architecture",
        groupUA: "КП-51м",
        nameUA: "Кисельов Євгеній Леонідович",
        themeUA:
          "Метод аналізу стратегії неперервної інтеграції програмного забезпечення при використанні архітектури мікросервісів",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/master/Kyselov_KP_51m.pdf",
      },
      {
        groupEN: "KP-51m",
        nameEN: "Andriy Letsyk",
        themeEN:
          "The combined statistical method of automazed spelling correction of natural language text data",
        groupUA: "КП-51м",
        nameUA: "Лецик Андрій Олександрович",
        themeUA:
          "Комбінований статистичний метод автоматизованої орфокорекції природномовних текстових даних",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/master/Letsyk_KP_51m.pdf",
      },
      {
        groupEN: "KP-51m",
        nameEN: "Roman Papusha",
        themeEN: "Method of raster lines smoothing",
        groupUA: "КП-51м",
        nameUA: "Папуша Роман Олегович",
        themeUA: "Метод згладжування растрових ліній",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/master/Papusha_KP_51m.pdf",
      },
      {
        groupEN: "KP-52m",
        nameEN: "Mykhailo Beherskyi",
        themeEN:
          "Method of automated classification of text data on the basis of hybrid models",
        groupUA: "КП-52м",
        nameUA: "Бегерський Михайло Васильович",
        themeUA:
          "Метод автоматизованої класифікації текстових даних на основі гібридних моделей",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/master/Beherskyi_KP_52m.pdf",
      },
      {
        groupEN: "KP-52m",
        nameEN: "Andrii Datsenko",
        themeEN: "Short text documents clustering based on Neural networks",
        groupUA: "КП-52м",
        nameUA: "Даценко Андрій Сергійович",
        themeUA:
          "Метод кластеризації коротких текстових документів на основі нейронних мереж",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/master/Datsenko_KP_52m.pdf",
      },
      {
        groupEN: "KP-52m",
        nameEN: "Nycheporuk Oleksandr",
        themeEN:
          "The method for point counting of elliptic curves over the field GF(2^m)",
        groupUA: "КП-52м",
        nameUA: "Ничепорук Олександр Анатолійович",
        themeUA: "Метод обчислення порядку еліптичної кривої над полем GF(2^m)",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/master/Nycheporuk_KP_52m.pdf",
      },
      {
        groupEN: "KP-52m",
        nameEN: "Shershniev Oleksii",
        themeEN:
          "The method of dynamic distribution of tasks in cluster systems",
        groupUA: "КП-52м",
        nameUA: "Шершнєв Олексій Володимирович",
        themeUA: "Метод динамічного розподілення завдань в кластерних системах",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/master/Shershniev_KP_52m.pdf",
      },
      {
        groupEN: "KP-52m",
        nameEN: "Larysa Shcherbachenko",
        themeEN:
          "A method of building a recommendation system for distance learning system based on the user context",
        groupUA: "КП-52м",
        nameUA: "Щербаченко Лариса Василівна",
        themeUA:
          "Метод побудови рекомендаційної системи для дистанційного навчання, заснований на контексті користувача",
        link:
          "http://pzks.fpm.kpi.ua/diploma/2016-2017/master/Shcherbachenko_KP_52m.pdf",
      },
    ],
  },
];
