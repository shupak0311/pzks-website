import styled from "styled-components";

export const Footer = styled.footer`
  padding: 1em 0 0;
  background: #081e3f;
  /* text-align: center; */
  color: white;
`;

export const FooterContent = styled.div`
  margin: 0 auto;
  display:block;
  max-width: 62.5rem;
  width: 100%;
  padding-left: 0.9375rem;
  padding-right: 0.9375rem;
`;


export const FooterLink = styled.a`
  color: #F8C93E;
  border-bottom: 1px solid;
`

export const Copyright = styled.p`
  margin: 1em 0;
`
export const LogoContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`
export const Logo = styled.div`
  height: auto;

`