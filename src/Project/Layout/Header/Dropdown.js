import React, { useRef } from "react";
import { NavLink } from "react-router-dom";
import { useTranslation } from "react-i18next";
import useOnOutsideClick from "../../../shared/hooks/onOutsideClick";
import * as S from "./Styles";

const Dropdown = ({
  subMenuItems,
  relatedLinks,
  activeLinkItemId,
  setActiveLinkItemId,
}) => {
  const { t, i18n } = useTranslation();
  const $dropDownRef = useRef();

  useOnOutsideClick($dropDownRef, activeLinkItemId, () =>
    setActiveLinkItemId(null)
  );

  return (
    <S.Dropdown ref={$dropDownRef}>
      <S.DropdownContent>
        <S.Column>
          <S.SectionTitle regular={true}>
            {t("Dropdown.sectionTitle")}
          </S.SectionTitle>
          <S.Separator />
          <S.SubLinksContainer>
            {subMenuItems.map(({ title, titleEn, path }, index) => {
              const linkItemProps = { as: NavLink, exact: true, to: `${path}` };
              return (
                <S.SubLink
                  onClick={() => setActiveLinkItemId(null)}
                  key={index}
                >
                  <S.SubLinkText {...linkItemProps}>
                    {i18n.language === "en" ? titleEn : title}
                  </S.SubLinkText>
                </S.SubLink>
              );
            })}
          </S.SubLinksContainer>
        </S.Column>
        <S.Column>
          <S.SectionTitle regular={true}>
            {t("Dropdown.relatedLinks")}
          </S.SectionTitle>
          <S.Separator />
          <S.RelatedLinksContainer>
            {relatedLinks.map(({ linkName }, index) => (
              <S.SubLink key={index}>
                <S.SubLinkText {...linkItemProps}>{linkName}</S.SubLinkText>
              </S.SubLink>
            ))}
          </S.RelatedLinksContainer>
        </S.Column>
      </S.DropdownContent>
    </S.Dropdown>
  );
};

export default Dropdown;
