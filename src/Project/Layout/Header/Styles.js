import styled, { css } from "styled-components";

export const GlobalHeader = styled.div`
  height: 32px;
  position: static;
  background: white;
`;

export const GlobalHeaderContent = styled.div`
  margin: 0 auto;
  max-width: 62.5rem;
  width: 100%;
`;

export const GlobalHeaderColumns = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
export const Title = styled.a`
  font-weight: bold;
  color: #081e3f;
  font-size: 0.8rem;
  text-transform: uppercase;
  padding: 10px 10px;
`;

export const FlagsContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
`;
export const Header = styled.header`
  height: 80px;
  width: 100%;
  background-color: #081e40;
  display: flex;
  position: relative;
`;

export const Logo = styled.div`
  ${(props) => css`
    background-image: url(${props.img});
  `}
  background-size: cover;
  background-repeat: no-repeat;
  width: 110px;
  height: 50px;
  flex: 0 0 auto;
`;

export const Navbar = styled.nav`
  width: 1000px;
  margin: 0 auto;
  display: flex;
  height: 100%;
  align-items: center;
`;

export const LinksList = styled.ul`
  display: flex;
  list-style: none;
  /* margin-left: 25px; */
  height: 100%;
`;

export const Link = styled.li`
  padding: 0 10px;
  height: 100%;
  display: flex;
  align-items: center;
  cursor: pointer;
  position: relative;

  &:after {
    content: "";
    position: absolute;
    bottom: ${(props) => (props.active ? "-14px" : "0")};
    transition: 0.15s ease bottom;
    border-style: solid;
    border-width: 15px 12px 0 12px;
    border-color: ${(props) =>
      props.active
        ? "#e8cd7d transparent transparent transparent"
        : "transparent"};
    transform: translateX(-50%);
    left: 50%;
    z-index: 100;
  }

  ${(props) =>
    props.active
      ? css`
          background-color: #e8cd7d;

          &:hover {
          }
        `
      : css`
          &:hover {
            background-color: #112d54;

            span {
              border-bottom: 2px solid #fff;
            }
          }
        `}
`;

export const LinkText = styled.span`
  color: #fff;
  border-bottom: 2px solid transparent;
  text-align: center;
  padding-bottom: 2.5px;
  font-size: 0.96rem;

  ${(props) =>
    props.active &&
    css`
      color: #081e3f;
    `}
`;

// You may extract it into a separate file

export const Dropdown = styled.div`
  background-color: #fff;
  position: absolute;
  min-width: 100%;
  top: auto;
  left: -100000px;
  right: -100000px;
  width: auto;
  overflow: visible;
  clip: auto;
  visibility: visible;
  z-index: 99;
  min-height: 210px;
`;

export const DropdownContent = styled.div`
  width: 1000px;
  margin: 0 auto;
  padding: 30px;
  display: flex;
  justify-content: space-between;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  width: 40%;
`;

export const SectionTitle = styled.h2`
  margin: 0 0 0.7rem 0;
  font-size: ${(props) => (props.regular ? "1.15rem" : "1.3rem")};
  font-weight: bold;
  color: #081e3f;
`;

export const InfoText = styled.p`
  font-size: 1rem;
  color: #000;
`;

export const Separator = styled.div`
  height: 1px;
  background-color: #ddd;
  margin: 0.2rem 0 1rem;
`;

export const SubLinksContainer = styled.ul`
  list-style: none;
  column-count: 2;
  column-gap: 10px;
  overflow: visible;
`;

export const SubLink = styled.li`
  margin-bottom: 7px;
  height: 100%;
  top: 0;
  background: transparent;
  width: 100%;
`;

export const SubLinkText = styled.a`
  color: #081e3f;
  padding-bottom: 3px;
  font-size: 0.9rem;
  overflow: visible;
  border-bottom: 2px solid transparent;
  display: inline-block;
  line-height: normal;
  white-space: normal;

  &:hover {
    border-bottom: 2px solid #081e3f;
  }
`;

export const RelatedLinksContainer = styled.ul`
  display: flex;
  flex-direction: column;
  list-style: none;
`;
