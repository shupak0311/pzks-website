export const headerMenuItems = [
  {
    id: 1,
    title: "Про кафедру",
    titleEn: "General Info",
    subMenuItems: [
      {
        title: "Загальна інформація",
        titleEn: "Overview",
        path: "/overview",
      },
      {
        title: "Кадровий Склад",
        titleEn: "Staff",
        path: "/staff",
      },
      {
        title: "Контакти",
        titleEn: "Contacts",
        path: "/contacts",
      },
    ],
  },
  {
    id: 2,
    title: "Вступ",
    titleEn: "Degree Programs",
    subMenuItems: [
      {
        title: "Інформація про спеціальність",
        titleEn: "General Info",
        path: "/general-info",
      },
      {
        title: "Підготовчі курси",
        titleEn: "Preparatory Courses",
        path: "/preparatory-courses",
      },
      {
        title: "Магістратура",
        titleEn: "Master",
        path: "/master",
      },
      {
        title: "Прийом на перший курс",
        titleEn: "Bachelor Program",
        path: "/bachelor-program",
      },
      {
        title: "Аспірантура",
        titleEn: "Postgraduate School",
        path: "/postgraduate-school",
      },
      {
        title: "Офіційні документи",
        titleEn: "Official Documents",
        path: "official-documents",
      },
    ],
  },
  {
    id: 3,
    title: "Навчальний процес",
    titleEn: "Educational Process",
    subMenuItems: [
      {
        title: "Консультации",
        titleEn: "Tutorial Time",
        path: "/tutorial-time",
      },
      {
        title: "Розклад занять",
        titleEn: "Timetable",
        path: "/timetable",
      },
      {
        title: "Розклад іспитів",
        titleEn: "Examinations",
        path: "/examinations",
      },
      {
        title: "Атестації",
        titleEn: "Assessment",
        path: "/assessment",
      },
      {
        title: "Практика",
        titleEn: "Internship",
        path: "/internship",
      },
      {
        title: "Випускні роботи",
        titleEn: "Graduation Paper",
        path: "graduation-paper",
      },
    ],
  },
  {
    id: 4,
    title: "Інформаційний пакет",
    titleEn: "Curriculum Information",
    subMenuItems: [
      {
        title: "Навчальний план",
        titleEn: "Curriculum Info",
        path: "/curriculum-info",
      },
    ],
  },
  {
    id: 5,
    title: "Наукова діяльність",
    titleEn: "Research",
    subMenuItems: [
      {
        title: "Наукова діяльність",
        titleEn: "Research",
        path: "/research",
      },
      {
        title: "Архів публікацій",
        titleEn: "Publications",
        path: "/publications",
      },
    ],
  },
  {
    id: 6,
    title: "Міжнародна діяльність",
    titleEn: "International Collaborations",
    subMenuItems: [
      {
        title: "Міжнародна діяльність",
        titleEn: "International Collaborations",
        path: "/international-collaborations",
      },
      {
        title: "Проект MEDIS",
        titleEn: "Project MEDIS",
        path: "/project-medis",
      },
      {
        title: "Проект PARIS",
        titleEn: "Project PARIS",
        path: "/project-paris",
      },
    ],
  },
  {
    id: 7,
    title: "Компанії партнери",
    titleEn: "Partner Companies",
    subMenuItems: [
      {
        title: "Компанії-партнери",
        titleEn: "Partner Companies",
        path: "/partner-companies",
      },
    ],
  },
  {
    id: 8,
    title: "Центр електронної освіти",
    titleEn: "E-Education Center",
    subMenuItems: [
      {
        title: "Про Центр електронної освіти",
        titleEn: "E-Education Center",
        path: "/education-center",
      },
    ],
  },
];
