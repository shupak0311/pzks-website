import React, { Fragment, useState, useEffect } from "react";
import { NavLink, Redirect, useRouteMatch, withRouter } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { headerMenuItems } from "./header.static.data";
import * as S from "./Styles";
import Dropdown from "./Dropdown";

import { Dropdown as SemanticDropdown } from "semantic-ui-react";

const languageOptions = [
  { key: "ua", text: "Українська", value: "ua" },
  { key: "en", text: "English", value: "en" },
];

const Header = withRouter(({ history }) => {
  const { t, i18n } = useTranslation();
  const [activeLinkItemId, setActiveLinkItemId] = useState(null);

  const changeLanguage = async (e, data) => {
    if (data.value) {
      localStorage.setItem("language", data.value);
      await i18n.changeLanguage(data.value);
    }
  };

  const renderLinkItem = ({ titleEn, title, id }) => {
    const active = id === activeLinkItemId;

    return (
      <S.Link key={id} onClick={() => setActiveLinkItemId(id)} active={active}>
        <S.LinkText active={active}>
          {i18n.language === "en" ? titleEn : title}
        </S.LinkText>
      </S.Link>
    );
  };

  const activeLink = headerMenuItems.find(
    (item) => item.id === activeLinkItemId
  );
  const { subMenuItems, relatedLinks } = activeLink ? activeLink : {};

  return (
    <Fragment>
      <S.GlobalHeader>
        <S.GlobalHeaderContent>
          <S.GlobalHeaderColumns>
            <S.Title onClick={() => history.push("/")}>
              {t("Header.title")}
            </S.Title>

            <SemanticDropdown
              style={{ marginBottom: "4px", marginRight: "10px" }}
              floating
              onChange={changeLanguage}
              options={languageOptions}
              text={`${t("Header.chooseLanguage")}`}
            />
          </S.GlobalHeaderColumns>
        </S.GlobalHeaderContent>
      </S.GlobalHeader>
      <S.Header>
        <S.Navbar>
          <S.LinksList>
            {headerMenuItems.map((item) => renderLinkItem(item))}
          </S.LinksList>
        </S.Navbar>
      </S.Header>
      {activeLinkItemId && (
        <Dropdown
          subMenuItems={subMenuItems || []}
          relatedLinks={relatedLinks || []}
          activeLinkItemId={activeLinkItemId}
          setActiveLinkItemId={setActiveLinkItemId}
        />
      )}
    </Fragment>
  );
});

export default Header;
