import React, { Fragment } from "react";
import { Grid } from "semantic-ui-react";
import { useTranslation } from "react-i18next";
import Logo from "../../../App/assets/images/logo_small.png";
import * as S from "../Styles";

import { Header, List, Image } from "semantic-ui-react";

const Footer = () => {
  const { t, i18n } = useTranslation();

  return (
    <Fragment>
      <S.Footer>
        <S.FooterContent>
          <Grid columns={3}>
            <Grid.Row>
              <Grid.Column>
                <List color>
                  <List.Item>
                    <Header as="h3" style={{ color: "white" }}>
                      {i18n.language === "en" ? "General Info" : "Про кафедру"}
                    </Header>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>
                      {i18n.language === "en"
                        ? "Overview"
                        : "Загальна інформація"}
                    </S.FooterLink>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>
                      {i18n.language === "en" ? "Staff" : "Кадровий Склад"}
                    </S.FooterLink>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>
                      {i18n.language === "en"
                        ? "Research"
                        : "Наукова діяльність"}
                    </S.FooterLink>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>
                      {i18n.language === "en"
                        ? "International Collaborations"
                        : "Міжнародна діяльність"}
                    </S.FooterLink>
                  </List.Item>
                </List>
              </Grid.Column>

              <Grid.Column>
                <List color>
                  <List.Item>
                    <Header as="h3" style={{ color: "white" }}>
                      {i18n.language === "en" ? "Study" : "Навчання"}
                    </Header>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>
                      {i18n.language === "en"
                        ? "Curriculum Info"
                        : "Навчальний план"}
                    </S.FooterLink>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>
                      {i18n.language === "en"
                        ? "Tutorial Time"
                        : "Розклад консультацій"}
                    </S.FooterLink>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>
                      {i18n.language === "en" ? "Timetable" : "Розклад занять"}
                    </S.FooterLink>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>
                      {i18n.language === "en"
                        ? "Examinations"
                        : "Розклад іспитів"}
                    </S.FooterLink>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>
                      {i18n.language === "en" ? "Internship" : "Практика"}
                    </S.FooterLink>
                  </List.Item>
                </List>
              </Grid.Column>

              <Grid.Column>
                <S.LogoContainer>
                  <S.Logo>
                    <Image src={Logo}></Image>
                  </S.Logo>
                  <Header
                    as="h6"
                    style={{
                      color: "white",
                      marginLeft: "10px",
                      fontSize: "13px",
                      marginTop: "5px",
                    }}
                  >
                    {i18n.language === "en"
                      ? "Computer Systems Software Department"
                      : "Кафедра програмного забезпечення комп'ютерних систем"}
                  </Header>
                </S.LogoContainer>
                <List>
                  <List.Item>
                    <Header as="h3" style={{ color: "white" }}>
                      {i18n.language === "en" ? "Contacts" : "Контакти"}
                    </Header>
                  </List.Item>
                  <List.Item>
                    {i18n.language === "en"
                      ? "PZKS department, FPM, pr. Peremogy, 37, Kyiv, Ukraine, 03056"
                      : "03056, Київ, пр. Перемоги, 37, ФПМ, кафедра ПЗКС"}
                  </List.Item>
                  <List.Item>(044) 204-9113</List.Item>
                </List>
                {/* <List color>
                  <List.Item>
                    <Header as="h3" style={{ color: "white" }}>
                      Про кафедру
                    </Header>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>Загальна інформація</S.FooterLink>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>Кадровий Склад</S.FooterLink>
                  </List.Item>
                  <List.Item href="#">
                    <S.FooterLink>Контакти</S.FooterLink>
                  </List.Item>
                </List> */}
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <S.Copyright>
            <small>
              © {i18n.language === "en" ? "PZKS department" : "Кафедра ПЗКС"}
            </small>
          </S.Copyright>
        </S.FooterContent>
      </S.Footer>
    </Fragment>
  );
};

export default Footer;
