import React, { Fragment, useEffect } from "react";
import {
  Route,
  Switch,
  Redirect,
  useRouteMatch,
  useHistory,
} from "react-router-dom";
import { Header, Footer } from "./Layout";
import { Overview, Staff, Contacts } from "./About";
import {
  BachelorProgram,
  GeneralInfo,
  Master,
  OfficialDocuments,
  PostgraduateSchool,
  PreparatoryCourses,
} from "./DegreePrograms";
import { GraduationPaper, Internship, Schedules } from "./EducationalProcess";
import {
  InternationalCollaborations,
  ProjectMedis,
  ProjectParis,
} from "./InternationalCollaborations";
import HomePage from "./HomePage";
import CurriculumInfo from "./CurriculumInfo";
import PublicationsArchive from "./PublicationsArchive";
import PartnerCompanies from "./PartnerCompanies";
import EducationCenter from "./EducationCenter";
import * as S from "./Styles";

const Project = () => {
  const match = useRouteMatch();
  const history = useHistory();

  return (
    <Fragment>
      <Header />

      <S.MainContentContainer>
        <Switch>
          <Route path={`${match.path}/`} render={() => <HomePage />} />

          <Route path={`/overview`} render={() => <Overview />} />
          <Route path={`/staff`} render={() => <Staff />} />
          <Route path={`/contacts`} render={() => <Contacts />} />

          <Route path={`/general-info`} render={() => <GeneralInfo />} />
          <Route
            path={`/preparatory-courses`}
            render={() => <PreparatoryCourses />}
          />

          <Route
            path={`/tutorial-time`}
            render={() => <Schedules path={"tutorial-time"} />}
          />
          <Route
            path={`/timetable`}
            render={() => <Schedules path={"timetable"} />}
          />
          <Route
            path={`/examinations`}
            render={() => <Schedules path={"examinations"} />}
          />
          <Route
            path={`/assessment`}
            render={() => <Schedules path={"assessment"} />}
          />

          <Route path={`/master`} render={() => <Master />} />
          <Route
            path={`/bachelor-program`}
            render={() => <BachelorProgram />}
          />
          <Route
            path={`/postgraduate-school`}
            render={() => <PostgraduateSchool />}
          />
          <Route
            path={`/official-documents`}
            render={() => <OfficialDocuments />}
          />
          <Route path={`/internship`} render={() => <Internship />} />
          <Route path={"/curriculum-info"} render={() => <CurriculumInfo />} />

          <Route
            path={"/international-collaborations"}
            render={() => <InternationalCollaborations />}
          />
          <Route path={"/project-medis"} render={() => <ProjectMedis />} />
          <Route path={"/project-paris"} render={() => <ProjectParis />} />
          <Route
            path={"/publications"}
            render={() => <PublicationsArchive />}
          />

          <Route
            path={"/graduation-paper"}
            render={() => <GraduationPaper />}
          />

          <Route
            path={"/partner-companies"}
            render={() => <PartnerCompanies />}
          />

          <Route
            path={"/education-center"}
            render={() => <EducationCenter />}
          />
        </Switch>
      </S.MainContentContainer>
      <Footer />
    </Fragment>
  );
};

export default Project;
