import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  /* padding: 0 5%;  */
  background-color: #f4f4f4;
`

export const Content = styled.div`
    height: 100%;
    display: block;
    margin: 0 auto;
    max-width: 62.5rem;
    padding: 20px 0;
`

export const Heading = styled.h2`
    font-size: 25px;
    color: #081E3f;
    margin-bottom: 8px;
`

export const Columns = styled.div`
  position: relative;
  padding-left: 0.9375rem;
  padding-right: .9375rem;
`

export const Text = styled.p`
    font-size: 16px;
    line-height: 1.6;
    font-weight: normal;
`