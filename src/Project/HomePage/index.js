import React, { Fragment } from "react";
import { useTranslation } from "react-i18next";
import Banner from "@Components/Banner";
import BannerBackGroundImg from "../../App/assets/images/kpi.png";
import Wrapper from "@Components/Wrapper";
import { Link } from "../Styles";
import { Header } from "semantic-ui-react";

import * as S from "./Styles";

const HomePage = () => {
  const { t } = useTranslation();

  return (
    <Fragment>
      <Banner
        title={<div>{t("HomePage.bannerTitle")}</div>}
        subtitle={t("HomePage.bannerSubtitle")}
        backgroundImg={BannerBackGroundImg}
      />

      <Wrapper>
        <div>
          <S.Heading>{t("HomePage.heading")}</S.Heading>
          <S.Text>
            {" "}
            {t("HomePage.text1")}
            <Link href="http://fpm.kpi.ua/" target="_blank">
              {t("HomePage.link")}
            </Link>{" "}
            {t("HomePage.text2")}
          </S.Text>
        </div>

        <Header as="h2">{t("HomePage.newsHeader")}</Header>
      </Wrapper>
    </Fragment>
  );
};

export default HomePage;
