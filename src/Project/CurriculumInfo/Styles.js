import styled from "styled-components";

export const Container = styled.div`
  max-width: 1024px;
  margin: 15px auto 0 auto;
`;

export const TableWrapper = styled.div`
  margin-bottom: 15px;
`;