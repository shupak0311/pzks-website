import React from "react";
import { Table } from "semantic-ui-react";
import { curriculumData } from "./data";
import * as S from "./Styles";
import Title from "../../shared/components/Title";
import Wrapper from "../../shared/components/Wrapper";
import Breadcrumbs from "../../shared/components/Breadcrumbs";

import {
  Container as SemanticContainer,
  Message,
  Header,
  Divider,
  Segment,
  List,
} from "semantic-ui-react";

const CurriculumInfo = () => {
  const data = curriculumData;
  const { bachelors, mastersProfessional, mastersScientific } = data;

  const renderTable = (tableTitle, tData) => {
    const firstTermSubjects = tData["1"] ? tData["1"].subjects : [];
    const secondTermSubjects = tData["2"] ? tData["2"].subjects : [];
    const maxSubjectsNumber = Math.max(
      firstTermSubjects.length,
      secondTermSubjects.length
    );

    const rows = getTableRows(
      maxSubjectsNumber,
      firstTermSubjects,
      secondTermSubjects
    );

    return (
      <S.TableWrapper>
        <Title title={tableTitle} />
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell textAlign={"center"}>
                Перший семестр
              </Table.HeaderCell>
              <Table.HeaderCell />
              <Table.HeaderCell textAlign={"center"}>
                Другий семестр
              </Table.HeaderCell>
              <Table.HeaderCell />
            </Table.Row>
            <Table.Row>
              <Table.HeaderCell textAlign={"center"}>
                Назва дисципліни
              </Table.HeaderCell>
              <Table.HeaderCell textAlign={"center"}>Кредити</Table.HeaderCell>
              <Table.HeaderCell textAlign={"center"}>
                Назва дисципліни
              </Table.HeaderCell>
              <Table.HeaderCell textAlign={"center"}>Кредити</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>{rows.map((r) => r)}</Table.Body>
          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell>Total</Table.HeaderCell>
              <Table.HeaderCell textAlign={"center"}>
                {tData["1"].totalCredits}
              </Table.HeaderCell>
              <Table.HeaderCell>Total</Table.HeaderCell>
              <Table.HeaderCell textAlign={"center"}>
                {tData["2"] ? tData["2"].totalCredits : ""}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      </S.TableWrapper>
    );
  };

  const getTableRows = (
    maxSubjectsNumber,
    firstTermSubjects,
    secondTermSubjects
  ) => {
    const res = [];

    for (let i = 0; i < maxSubjectsNumber; i++) {
      const ftSub = firstTermSubjects[i];
      const stSub = secondTermSubjects[i];

      res.push(
        <Table.Row key={i}>
          <Table.Cell>{ftSub ? ftSub.subjectEN : ""}</Table.Cell>
          <Table.Cell textAlign={"center"}>
            {ftSub ? ftSub.credits : ""}
          </Table.Cell>
          <Table.Cell>{stSub ? stSub.subjectEN : ""}</Table.Cell>
          <Table.Cell textAlign={"center"}>
            {stSub ? stSub.credits : ""}
          </Table.Cell>
        </Table.Row>
      );
    }

    return res;
  };

  const formDegreeData = (degreeData) => {
    const res = {};

    degreeData.forEach((subject) => {
      const { year, term, credits } = subject;

      if (!res[year]) {
        res[year] = {};
      }

      if (!res[year][term]) {
        res[year][term] = {
          totalCredits: 0,
          subjects: [],
        };
      }

      // add subject
      res[year][term]["subjects"].push(subject);
      // count total credits
      res[year][term]["totalCredits"] += credits;
    });

    return res;
  };

  const renderInfo = () => {
    const bachelorsData = formDegreeData(bachelors);
    const mastersProfessionalData = formDegreeData(mastersProfessional);
    const mastersScientificData = formDegreeData(mastersScientific);

    return (
      <Wrapper>
        <Breadcrumbs />
        <Title title={"Навчальний план"} />

        <Header as="h1" color="blue" textAlign="center">
          Спеціальність Інженерія програмного забезпечення
        </Header>

        <Header as="h2" textAlign="center" dividing>
          СТРУКТУРА БАКАЛАВРСЬКОЇ ПРОГРАМИ
        </Header>
        {Object.entries(bachelorsData).map(([year, term]) => {
          return renderTable(`${year} рік навчання`, term);
        })}

        <Header as="h2" textAlign="center" dividing>
          СТРУКТУРА МАГІСТЕРСЬКОЇ ПРОГРАМИ
        </Header>

        <Header as="h3" textAlign="center">
          ОСВІТНЬО-ПРОФЕСІЙНА ПРОГРАМА
        </Header>
        {Object.entries(mastersProfessionalData).map(([year, term]) => {
          return renderTable(`${year} рік навчання`, term);
        })}

        <Header as="h3" textAlign="center">
          ОСВІТНЬО-НАУКОВА ПРОГРАМА
        </Header>
        {Object.entries(mastersScientificData).map(([year, term]) => {
          return renderTable(`${year} рік навчання`, term);
        })}
      </Wrapper>
    );
  };

  return <S.Container>{renderInfo()}</S.Container>;
};

export default CurriculumInfo;
