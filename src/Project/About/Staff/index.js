import React, { Fragment } from "react";
import {
  Container,
  Content,
  Columns,
  RightContent,
  FlexContainer,
} from "../../Styles";
import SideNav from "@Components/Navigation";
import Title from "@Components/Title/index";
import Paragraph from "@Components/Paragraph/index";
import Breadcrumbs from "@Components/Breadcrumbs";
import TeacherCard from "@Components/TeacherCard";
import { Header } from "semantic-ui-react";
import { teachers } from "./teachers.static.data";
import * as S from "./Styles";

const chunks = [
  "Основою якісної підготовки фахівців у будь-якому навчальному закладі є викладачі, адже, як зазначав великий Айнштайн: «Найвище мистецтво викладача — пробудити задоволення від творчого вираження та знань».",
  "На кафедрі працюють висококваліфіковані викладачі, інформацію про яких можна знайти в цьому розділі.",
];

const TeachersBlock = ({ teachers }) => (
  <S.TeachersBlock style={{ marginBottom: "80px" }}>
    {teachers.map(({ img, name, position, url }, index) => (
      <TeacherCard
        key={index}
        img={img}
        name={name}
        position={position}
        link={url}
      />
    ))}
  </S.TeachersBlock>
);

const Staff = () => {
  return (
    <Fragment>
      <Container>
        <Content>
          <Columns>
            <Breadcrumbs />
            <Title title={"Кадровий Склад"} />
            <FlexContainer>
              <SideNav />
              <RightContent>
                <Paragraph chunks={chunks} />
              </RightContent>
            </FlexContainer>

            {/* <TeachersBlock teachers={teachers} /> */}

            <Header as="h3" dividing>
              Науковий керівник кафедри
            </Header>
            <TeacherCard
              img={
                "http://pzks.fpm.kpi.ua/images/stories/NewImages/Teachers/dichka.jpg"
              }
              name={"Дичка Іван Андрійович"}
              position={"д.т.н., професор"}
              link={"http://intellect.pzks.fpm.kpi.ua/profile/dia7"}
            />

            <Header as="h3" dividing>
              В. о. завідувача кафедри
            </Header>
            <TeacherCard
              img={
                "http://pzks.fpm.kpi.ua/images/stories/NewImages/Teachers/leheza.jpg"
              }
              name={"Легеза Віктор Петрович"}
              position={"д.т.н., професор"}
              link={"http://intellect.pzks.fpm.kpi.ua/profile/lvp14"}
            />

            <Header as="h3" dividing>
              Викладачі
            </Header>
            <TeachersBlock
              teachers={teachers}
              style={{ marginBottom: "20px" }}
            />
          </Columns>
        </Content>
      </Container>
    </Fragment>
  );
};

export default Staff;
