export const teachers = [
  {
    img: "http://pzks.fpm.kpi.ua/images/stories/NewImages/Teachers/sulema_yevgeniya-photo_small.jpg",
    name: "Сулема Євгенія Станіславівна",
    position: "к.т.н., доцент",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/ses12",
  },
  {
    img: "http://pzks.fpm.kpi.ua/images/stories/NewImages/Teachers/zabolotnia.jpg",
    name: "Заболотня Тетяна Миколаївна",
    position: "к.т.н., доцент",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/ztm3",
  },
  {
    img: "http://pzks.fpm.kpi.ua/images/stories/onai_new.jpg",
    name: "Онай Микола Володимирович",
    position: "к.т.н., доцент",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/omv16",
  },
  {
    img: "https://i.ibb.co/6PCFSmK/image.jpg",
    name: "Олещенко Любов Михайлівна",
    position: "к.т.н., доцент",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/olm5",
  },
  {
    img: "http://pzks.fpm.kpi.ua/images/stories/dsc02855_.jpg",
    name: "Рибачок Наталія Антонівна",
    position: "к.т.н., старший викладач",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/rna4",
  },
  {
    img: "http://pzks.fpm.kpi.ua/images/stories/NewImages/Teachers/lesya_.jpg",
    name: "Люшенко Леся Анатоліївна",
    position: "к.т.н., старший викладач",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/lla1",
  },
  {
    img: "http://pzks.fpm.kpi.ua/images/stories/NewImages/Teachers/zhabina.jpg",
    name: "Жабіна Валентина Валеріївна",
    position: "к.т.н., доцент",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/zvv45",
  },
  {
    img: "https://i.ibb.co/f1r6zfm/user-profile-31922.jpg",
    name: "	Юрчишин Василь Якович",
    position: "с.н.с, к.т.н., доцент",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/yvy",
  },
  {
    img: "https://i.ibb.co/9GXd3fN/0123456.png",
    name: "Хіцко Яна Володимирівна",
    position: "к.т.н., старший викладач",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/xyv2",
  },
  {
    img: "http://pzks.fpm.kpi.ua/images/stories/NewImages/Teachers/buhtiyarov.jpg",
    name: "Бухтіяров Юрій Вікторович",
    position: "старший викладач",
    url: "http://intellect.pzks.fpm.kpi.ua/profile/byv18",
  },
];
