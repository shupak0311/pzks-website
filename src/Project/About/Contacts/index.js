import React, { Fragment } from "react";
import {
  Container,
  Content,
  Columns,
  RightContent,
  FlexContainer,
} from "../../Styles";
import SideNav from "@Components/Navigation";
import Title from "@Components/Title";
import Breadcrumbs from "@Components/Breadcrumbs";

import { List } from "semantic-ui-react";

const Contact = () => {
  return (
    <Fragment>
      <Container>
        <Content>
          <Columns>
            <Breadcrumbs />
            <Title title={"Контакти"} />
            <SideNav />
            <RightContent>
              <List>
                <List.Item>
                  <List.Icon name="envelope outline" size="big" />
                  <List.Content style={{ paddingTop: "7px" }}>
                    Адреса для листування: 03056, Київ, пр. Перемоги, 37, ФПМ,
                    кафедра ПЗКС
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Icon name="phone" size="big" />
                  <List.Content style={{ paddingTop: "7px" }}>
                    Телефон: (044) 204-9113
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Icon name="fax" size="big" />
                  <List.Content style={{ paddingTop: "7px" }}>
                    Факс: (044) 204-8115
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Icon name="mail" size="big" />
                  <List.Content style={{ paddingTop: "7px" }}>
                    Email:{" "}
                    <a
                      style={{ color: "#4183c4" }}
                      href="mailto:dychka@pzks.fpm.kpi.ua "
                    >
                      dychka@pzks.fpm.kpi.ua
                    </a>
                    ,{" "}
                    <a
                      style={{ color: "#4183c4" }}
                      href="mailto:sulema@pzks.fpm.kpi.ua"
                    >
                      sulema@pzks.fpm.kpi.ua
                    </a>
                  </List.Content>
                </List.Item>
              </List>
            </RightContent>
          </Columns>
        </Content>
      </Container>
    </Fragment>
  );
};

export default Contact;
