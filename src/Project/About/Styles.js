import styled from "styled-components";

export const HeadingPrimary = styled.h1`
  font-size: 2.75rem;
  font-weight: bold;
  color: #081e3f;
  margin-top: 18px;
`;

export const Text = styled.span`
  display: block;
`;

export const RightContent = styled.div`
  width: 75%;
  padding: 0 10px;
  float: right;
`;