import React, { Fragment } from "react";
import { useRouteMatch } from "react-router-dom";
import { parseURL } from "../../../shared/utils/helpers";
import {
  Container,
  Content,
  Columns,
  RightContent,
  FlexContainer,
} from "../../Styles";
import SideNav from "@Components/Navigation";
import Title from "@Components/Title";
import Paragraph from "@Components/Paragraph";
import TextList from "@Components/TextList";
import Breadcrumbs from "@Components/Breadcrumbs";

const chunks = [
  "Кафедра програмного забезпечення комп’ютерних систем (ПЗКС) факультету прикладної математики створена у 2009 році. Метою діяльності кафедри є підготовка фахівців програмної інженерії, а також науково-дослідна робота у галузі програмного забезпечення комп'ютерних систем.",
  "Науковий керівник кафедри – д.т.н., професор Дичка Іван Андрійович. В. о. завідувача кафедри - д.т.н., професор Легеза Віктор Петрович. Заступник завідувача кафедри - к.т.н., доцент Сулема Євгенія Станіславівна. Учений секретар кафедри - к.т.н. Олещенко Любов Михайлівна",
  "На кафедрі працюють 3 доктори наук та 14  кандидатів наук.",
  "Кафедра розташована у 14 та 15 корпусах 'КПІ ім. Ігоря Сікорського'",
];

const textListChunksUpper = [
  "к. 03 (1 поверх) – комп’ютерний клас,",
  "к. 40а(1 поверх) – викладацька,",
  "к. 67 (4 поверх) – комп’ютерний клас,",
  "к. 62 (5 поверх) – викладацька,",
  "к. 70 (5 поверх) – кабінет завідувача лабораторій, серверна,",
  "к. 73 (5 поверх) – кабінет завідувача кафедри,",
  "к. 76 (5 поверх) – комп’ютерна лабораторія.",
];

const textListChunksBottom = [
  "к.015/2(1 поверх) – спеціалізована навчальна аудиторія кафедри",
  "к. 111 (5 поверх) – Центр електронної освіти.",
];

const About = () => {
  const match = useRouteMatch();
  const breadcrumbs = parseURL(match.path);

  return (
    <Fragment>
      <Container>
        <Content>
          <Columns>
            <Breadcrumbs />
          </Columns>
          <Title title={"Загальна інформація"} />
        </Content>
        <FlexContainer>
          <SideNav />
          <RightContent>
            <Paragraph chunks={chunks} />
          </RightContent>
        </FlexContainer>
        <div style={{ marginTop: "15px", marginBottom: "80px" }}>
          <Title title={"Корпуси"} />
          <FlexContainer>
            <div>
              <TextList
                headerText={"Приміщення кафедри ПЗКС у 14 корпусі:"}
                items={textListChunksUpper}
              />
              <TextList
                headerText={"Приміщення кафедри ПЗКС у 15 корпусі:"}
                items={textListChunksBottom}
              />
            </div>
            <div>
              <img
                src={
                  "http://pzks.fpm.kpi.ua/images/stories/NewImages/img_4914_2%20-%20.jpg"
                }
              />
            </div>
          </FlexContainer>
        </div>
      </Container>
    </Fragment>
  );
};

export default About;
