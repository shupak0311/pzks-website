import styled from "styled-components";


export const Container = styled.div`
  width: 100%;
  /* padding: 0 3%;  media query*/ 
  max-width: 62.5rem;
  margin: 0 auto;
`

export const Content = styled.div`
    height: 100%;
    display: block;
    padding: 20px 0;
`

export const Columns = styled.div`
  position: relative;
  padding-right: .9375rem;
`

export const FlexContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;  
`

export const RightContent = styled.div`
  width: 75%;
  padding: 0 10px;
  float: right;
`;


export const Flex = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

export const FlexColumn = styled.div`
  display: flex;
  flex-direction: column;
  align-items: baseline;
  justify-content: flex-start;
`


export const Link = styled.a`
color: blue;

 &:hover {
  color: blue;
  text-decoration: underline;
}
`
export const MainContentContainer = styled.div`
  min-height: 85vh;
`