import React, { Fragment } from "react";
import {
  Container,
  Content,
  Columns,
  FlexContainer,
  Flex,
  Link,
} from "../../Styles";
import Title from "@Components/Title/";
import Breadcrumbs from "@Components/Breadcrumbs";
import { Icon, List, Flag } from "semantic-ui-react";
import Wrapper from "@Components/Wrapper";
import TeacherCard from "@Components/TeacherCard";

import {
  Container as SemanticContainer,
  Divider,
  Message,
  Segment,
  Header,
  Image,
} from "semantic-ui-react";

const ProjectParis = () => {
  return (
    <Wrapper>
      <Breadcrumbs />
      <Title title={"Проект ParIS"} />

      <SemanticContainer textAlign="left">
        <p>
          Проект ParIS (Партнерство з інформаційної безпеки) – це 3-річний
          проект стратегічного партнерства, що виконується в рамках програми
          Еразмус+ (Ключова дія 2) чотирма університетами:
        </p>
        <List divided relaxed>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="lu" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Університет Люксембургу (Люксембург)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="pt" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Університет Лісабону (Португалія)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="pl" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Варшавський технологічний університет «Варшавська політехніка»
                  (Польща)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="ua" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  КПІ ім. Ігоря Сікорського
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
        </List>
        <p style={{ marginTop: "20px" }}>
          Метою цього стратегічного партнерства є підготовка професіоналів у
          галузі ІКТ, які здатні ефективно застосовувати свої знання у
          предметній галузі для підвищення захисту та надійності інформаційних
          технологій. Для досягнення цієї мети університети-учасники проекту
          створюють міжнародну спільну магістерську програму (Joint Master’s
          Programme) з інформаційної безпеки (Information Security). В рамках
          виконання проекту також щорічно проводяться 5-денні інтенсивні
          тренінги за участю студентів Європейських університетів та викладачів
          партнерських університетів.
        </p>
        <p style={{ marginTop: "20px" }}>
          Участь кафедри ПЗКС полягає у розроблені методики викладання
          дисципліни «Захист мультимедійних даних» та академічній
          спів-координації проекту у КПІ ім. Ігоря Сікорського.
        </p>
        <Divider />
        <Flex style={{ marginTop: "20px" }}>
          <Icon name="time" size="large" />
          <p>
            <strong>Тривалість проекту:</strong> 1 грудня 2013 – 1 грудня 2016
            (36 місяців)
          </p>
        </Flex>
        <Divider />
        <strong>Очікувані результати:</strong> створення міжнародної спільної
        магістерської програми (Joint Master’s Programme).
        <Message info>
          <Message.Header>
            Детальна інформація про проект знаходиться за цим{" "}
            <a
              style={{ color: "blue" }}
              href="https://paris.uni.lu/"
              target="_blank"
            >
              посиланням.
            </a>
          </Message.Header>
        </Message>
      </SemanticContainer>
    </Wrapper>
  );
};

export default ProjectParis;
