import React from "react";
import { Flex } from "../../Styles";
import Title from "@Components/Title";
import Breadcrumbs from "@Components/Breadcrumbs";
import { Icon, List, Flag } from "semantic-ui-react";
import Wrapper from "@Components/Wrapper";
import TeacherCard from "@Components/TeacherCard";
import MedisLogo from "../../../App/assets/images/logotipo2.png";

import {
  Container as SemanticContainer,
  Divider,
  Message,
  Segment,
  Header,
  Image,
} from "semantic-ui-react";

const ProjectMedis = () => {
  return (
    <Wrapper>
      <Breadcrumbs />
      <Title title={"Проект MEDIS"} />

      <SemanticContainer textAlign="left">
        <Header as="h4" color="blue">
          Методологія підготовки висококваліфікованих інженерів магістерського
          рівня в галузі проектування та розроблення сучасних промислових
          інформаційних систем
        </Header>

        <SemanticContainer>
          <Image src={MedisLogo} size="large" floated="right" />
          <p>
            Основною метою проекту є інтеграція методології
            проблемно-орієнтованого навчання (Problem Based-Learning – PBL) в
            інженерні магістерські програми в країнах-партнерах з метою
            підготовки висококваліфікованих інженерів з розроблення сучасних
            розподілених промислових інформаційних систем на основі
            мікроконтролерів, промислових комп’ютерів, мобільних та хмарних
            комп’ютерних платформ. Цільовою группою є студенти, викладачі та
            адміністрація університетів.
          </p>
        </SemanticContainer>

        <Divider />

        <Segment raised color="blue" style={{ marginTop: "20px" }}>
          <p>Завданнями проекту є:</p>
          <List bulleted>
            <List.Item>
              запропонувати PBL-методологію та розробити ресурси для викладання
              запропонованого модуля спеціалізації сучасної промислової
              інформатики (Advanced Industrial Informatics Specialization Module
              – AIISM);
            </List.Item>
            <List.Item>
              проаналізувати навчальні плани університетів у країнах-партнерах
              та адаптувати AIISM з метою його інтеграції в специфічні навчальні
              програми у кожній країні-партері;
            </List.Item>
            <List.Item>
              розробити навчальні курси та підготувати викладачів, допоміжний та
              адміністративний персонал у країнах-партнерах;
            </List.Item>
            <List.Item>
              впровадити AIISM-PBL-методологію в країнах-партнерах та сприяти її
              подальшому застосуванню;
            </List.Item>
            <List.Item>оцінити результати впровадження AIISM;</List.Item>
            <List.Item>
              розповсюдити досвід та результати проекту серед зацікавлених
              сторін.
            </List.Item>
          </List>
        </Segment>

        <Divider />

        <Segment raised color="blue" style={{ marginTop: "20px" }}>
          <strong>Напрями діяльності за проектом:</strong>
          <List bulleted>
            <List.Item>Розроблення AIISM-PBL-методології</List.Item>
            <List.Item>Розроблення навчальних ресурсів для AIISM;</List.Item>
            <List.Item>
              Адаптація AIISM до специфіки навчальних планів у країнах-партнерах
            </List.Item>
            <List.Item>Підготовка викладачів та персоналу</List.Item>
            <List.Item>Впровадження та супровід AIISM</List.Item>
            <List.Item>Оцінка якості застосування AIISM</List.Item>
            <List.Item>Розповсюдження досвіду та результатів проекту</List.Item>
          </List>
        </Segment>

        <Divider />

        <Segment raised color="blue" style={{ marginTop: "20px" }}>
          <strong>Очікувані результати:</strong>
          <List ordered>
            <List.Item>Розроблена AIISM-PBL-методологія</List.Item>
            <List.Item>Розроблені навчальні ресурси для AIISM</List.Item>
            <List.Item>
              Документація щодо адаптації AIISM до специфіки навчальних програм
              в країніх-партнерах
            </List.Item>
            <List.Item>Розроблені навчальні курси</List.Item>
            <List.Item>
              Матеріали для поширення результатів проекту, включаючи публікації
              у журналах та на конференціях, звіт для поширення серед
              зацікавлених сторін, статті на web-сайті та у соціальних мережах
            </List.Item>
          </List>
        </Segment>

        <Divider />

        <Header as="h4">Вузи партнери:</Header>

        <List divided relaxed>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="se" />
                <List.Header>Університет Мелардалена, Швеція</List.Header>
              </Flex>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="bg" />
                <List.Header>Технічний університет Софії, Болгарія</List.Header>
              </Flex>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="de" />
                <List.Header>Університет Штутгарта, Німеччина</List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="pt" />
                <List.Header>Університет Порто, Португалія</List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="kz" />
                <List.Header>
                  Павлодарський державний університет, Казахстан
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="kz" />
                <List.Header>
                  Казахський національний університет імені аль-Фрабі, Казахстан
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="ua" />
                <List.Header>
                  Національний технічний університет України “Київський
                  політехнічний інститут імені Ігоря Сікорського”, Україна
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="ua" />
                <List.Header>
                  Одеський національний політехнічний університет, Україна
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="ru" />
                <List.Header>
                  Санкт-Петербурзький державний політехнічний університет,
                  Російська Федерація
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="ru" />
                <List.Header>
                  Петрозаводський державний університет, Російська Федерація
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
        </List>

        <Divider />

        <Flex style={{ marginTop: "20px" }}>
          <Icon name="time" size="large" />
          <p>
            <strong>Тривалість проекту:</strong> 1 грудня 2013 – 1 грудня 2016
            (36 місяців)
          </p>
        </Flex>

        <Divider />

        <Header as="h3">Координатор проекту від України</Header>
        <Flex>
          <TeacherCard
            img={
              "http://pzks.fpm.kpi.ua/images/stories/NewImages/Teachers/sulema_yevgeniya-photo_small.jpg"
            }
            name={"Сулема Євгенія Станіславівна"}
            position={"к.т.н., доцент"}
            link={"http://intellect.pzks.fpm.kpi.ua/profile/ses12"}
          />
          <div style={{ alignSelf: "flex-start", marginTop: "26px" }}>
            <Flex>
              <Icon name="phone" size="large" />
              Довідки за телефоном: +38-044-204-99-44{" "}
            </Flex>
            <Flex style={{ marginTop: "16px" }}>
              <Icon name="mail" size="large" />
              Email:{" "}
              <a
                style={{ color: "#4183c4" }}
                href="mailto:sulema@pzks.fpm.kpi.ua"
              >
                sulema@pzks.fpm.kpi.ua
              </a>
            </Flex>
            <Flex style={{ marginTop: "16px" }}>
              <Icon name="location arrow" size="large" />
              корпус 15, каб.111{" "}
            </Flex>
          </div>
        </Flex>

        <Divider />

        <Message info>
          <Message.Header>
            Детальна інформація про проект знаходиться за цим{" "}
            <a
              style={{ color: "blue" }}
              href="https://www.medis-tempus.eu/"
              target="_blank"
            >
              посиланням.
            </a>
          </Message.Header>
        </Message>
      </SemanticContainer>
    </Wrapper>
  );
};

export default ProjectMedis;
