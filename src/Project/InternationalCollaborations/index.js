import React from "react";
import {
  Flex,
  Link,
} from "../Styles";
import Title from "@Components/Title/index";
import Breadcrumbs from "@Components/Breadcrumbs";
import { List, Flag } from "semantic-ui-react";
import Wrapper from "@Components/Wrapper";
import ProjectMedis from "./ProjectMedis"
import ProjectParis from "./ProjectParis"

import {
  Container as SemanticContainer,
  Divider,
  Segment,
} from "semantic-ui-react";

const InternationalCollaborations = () => {
  return (
    <Wrapper>
      <Breadcrumbs />
      <Title title={"Міжнародна діяльність"} />

      <SemanticContainer textAlign="left">
        <p>
          Кафедра програмного забезпечення комп’ютерних систем співпрацює зі
          спорідненими кафедрами таких університетів:
        </p>

        <List divided relaxed>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="at" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Університет прикладних наук Карінтії (Австрія)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="es" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Політехнічний університет Валенсії (Іспанія),
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="lu" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Університет Люксембургу (Люксембург)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="pt" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Університет Лісабону (Португалія)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="pt" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Університет Порто (Португалія)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="pl" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Варшавський технологічний університет «Варшавська політехніка»
                  (Польща)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="pl" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Польсько-Японський інститут інформаційних технологій (Польща)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="bg" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Університет Софії (Болгарія)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <Flex>
                <Flag name="gr" />
                <List.Header
                  as="a"
                  href="http://www.fh-kaernten.at/en/"
                  target="blank"
                >
                  Університет Салонік (Греція)
                </List.Header>
              </Flex>
            </List.Content>
          </List.Item>
        </List>
      </SemanticContainer>

      <Segment raised color="blue" style={{ marginTop: "20px" }}>
        <p>
          В рамках міжнародного співробітництва з цими та іншими університетами
          кафедра ПЗКС бере участь у виконанні 2 міжнародних проектів:
        </p>
        <List ordered>
          <List.Item>
            <Link href="https://www.medis-tempus.eu/" target="_blank">Проект MEDIS</Link>
          </List.Item>
          <List.Item>
            <Link href="https://paris.uni.lu/" target="_blank">Проект ParIS</Link>
          </List.Item>
        </List>
      </Segment>

      <Divider/>

      <p>
        До складу кафедри входить{" "}
        <Link href="http://eec.kpi.ua/?page_id=33" target="_blank">
          {" "}
          Центр електронної освіти «КПІ ім. Ігоря Сікорського»
        </Link>
        , створений у співпраці з Польсько-Японським інститутом інформаційних
        технологій в рамках виконання проекту ПРООН «Передача ІТ в Україну».
      </p>
    </Wrapper>
  );
};

export { InternationalCollaborations, ProjectMedis, ProjectParis};
