import React from "react";
import Title from "../../shared/components/Title/index";
import Breadcrumbs from "../../shared/components/Breadcrumbs";
import { Link } from "../Styles";
import Wrapper from "../../shared/components/Wrapper";

import {
  Container as SemanticContainer,
  Divider,
  Segment,
  List,
} from "semantic-ui-react";

const EducationCenter = () => {
  return (
    <Wrapper>
      <Breadcrumbs />
      <Title title={"Центр електронної освіти"} />

      <SemanticContainer textAlign="left">
        <p>
          Центр електронної освіти (ЦЕО) КПІ ім. Ігоря Сікорського створено на
          базі факультету прикладної математики в 2005 році у рамках проекту.
          Передача інформаційних технологій в Україну” Програми Розвитку ООН
          (ПРООН) в Україні. В 2010 році ЦЕО увійшов до складу кафедри
          програмного забезпечення комп’ютерних систем.
        </p>

        <Divider />

        <p style={{ marginTop: "20px" }}>
          Метою діяльності ЦЕО є організація дистанційної форми навчання з
          сучасних інформаційних технологій з використанням навчальних
          матеріалів, наданих Польсько-Японським інститутом інформаційних
          технологій; організація відео-конференцій, семінарів, тренінгів та
          курсів з окремих дисциплін підготовки фахівців з інформаційних
          технологій (у рамках впровадження Проекту). В інформаційно-методичній
          базі{" "}
          <Link href="http://eec.kpi.ua/?page_id=33" target="_blank">
            ЦЕО
          </Link>{" "}
          наявні 47 дистанційних курсів.
        </p>
      </SemanticContainer>
    </Wrapper>
  );
};

export default EducationCenter;
