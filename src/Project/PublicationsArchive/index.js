import React, { Fragment, useState } from "react";
import {
  Container,
  Content,
  Columns,
  FlexContainer,
  Flex,
  Link,
} from "../Styles";
import Title from "../../shared/components/Title/index";
import Breadcrumbs from "../../shared/components/Breadcrumbs";
import { Icon, List, Flag } from "semantic-ui-react";
import Wrapper from "../../shared/components/Wrapper";

import {
  Container as SemanticContainer,
  Divider,
  Header,
  Button,
  Modal,
} from "semantic-ui-react";
import { data } from "./data";

const PublicationsArchive = () => {
  const [isModalOpen, setModalOpenState] = useState(false);
  const [archiveEntryYear, setArchiveEntryYear] = useState(null);

  const onBtnClick = (year) => {
    setArchiveEntryYear(year);
    setModalOpenState(true);
  };

  const assignOrdinalNumberToPublicationItems = (publications) => {
    let counter = 0;

    return publications.map((p) => {
      return {
        ...p,
        items: p.items.map((item) => {
          ++counter;
          return `${counter}. ${item}`;
        }),
      };
    });
  };

  const renderButtons = () => {
    return data.map((entry) => {
      const { year } = entry;

      return (
        <SemanticContainer>
          <Button primary
            onClick={() => onBtnClick(year)}
          >{`Розгорнути ${year} рік`}</Button>

          <Divider />
        </SemanticContainer>
      );
    });
  };

  const renderModal = () => {
    const archiveYearData = data.find(
      (entry) => entry.year === archiveEntryYear
    );
    const publications = assignOrdinalNumberToPublicationItems(
      archiveYearData.publications
    );

    return (
      <Modal
        size={"full"}
        dimmer="blurring"
        onClose={() => setModalOpenState(false)}
        open={isModalOpen}
      >
        <Modal.Header>{`Публікації за ${archiveEntryYear} рік`}</Modal.Header>
        <Modal.Content scrolling>
          {publications.map((p) => {
            return (
              <React.Fragment>
                <Header as="h4">{p.title}</Header>
                {p.items.map((item) => (
                  <p>{item}</p>
                ))}
              </React.Fragment>
            );
          })}
        </Modal.Content>
      </Modal>
    );
  };

  return (
    <Wrapper>
      <Breadcrumbs />
      <Title title={"Архів публікацій"} />
      <SemanticContainer textAlign="left">
        <div style={{ marginTop: "20px" }}>{renderButtons()}</div>
        {archiveEntryYear && renderModal()}
      </SemanticContainer>
    </Wrapper>
  );
};

export default PublicationsArchive;
