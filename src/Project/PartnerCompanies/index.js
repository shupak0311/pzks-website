import React from "react";
import Title from "../../shared/components/Title/index";
import Breadcrumbs from "../../shared/components/Breadcrumbs";
import Wrapper from "../../shared/components/Wrapper";

import {
  Container as SemanticContainer,
  Divider,
  Segment,
  List,
} from "semantic-ui-react";

const PartnerCompanies = () => {
  return (
    <Wrapper>
      <Breadcrumbs />
      <Title title={"Компанії-партнери"} />

      <SemanticContainer textAlign="left">
        <p>
          Кафедра ПЗКС співпрацює не лише з освітніми організаціями, а також з
          відомою ІТ-компанією ЕПАМ. Компанія ЕПАМ є лідером ІТ-консалтинга та
          розроблення програмного забезпечення для світового ринку. Компанія
          поєднує вітчизняну школу прикладних та фундаментальних досліджень з
          досвідом реалізації великих проектів та практичним знанням сучасного
          міжнародного бізнесу. Підрозділи ЕПАМ з розроблення, тестування та
          підтримки програмних продуктів є в США, Угорщині, Німеччині, Швеції,
          Швейцарії, Великій Британії, Росії, Білорусі та Україні.
        </p>

        <Divider />

        <p>
          Співробітництво між кафедрою ПЗКС та компанією ЕПАМ стосується як
          навчальної, так і науково-інноваційної діяльності. Зокрема компанія
          ЕПАМ є спонсором у створені навчальної та науково-дослідної
          лабораторії ЕПАМ-ФПМ, яка працює на базі кафедри ПЗКС (лаб. 76-15). В
          рамках роботи цієї лабораторії на постійній основі діє майстер-клас з
          програмної інженерії.
        </p>

        <Segment raised color="blue" style={{ marginTop: "20px" }}>
          <p>Майстер-клас включає такі тренінги:</p>
          <List bulleted>
            <List.Item>двомісячні курси з Java</List.Item>
            <List.Item>тренінг з тестування програмного забезпечення</List.Item>
          </List>
        </Segment>
      </SemanticContainer>

      <p style={{ marginTop: "20px" }}>
        Тренінги проводять досвідчені викладачі та лідери команд розробників
        компанії ЕПАМ.
      </p>
    </Wrapper>
  );
};

export default PartnerCompanies;
