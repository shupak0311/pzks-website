import React, { Fragment } from 'react';
import {hot} from 'react-hot-loader/root';
import NormalizeStyles from './NormalizeStyles';
import BaseStyles from './BaseStyles';
import Routes from './Routes';

import 'semantic-ui-css/semantic.min.css'
import "./styles.css"
import './fontStyles.css';

const App = () => (
  <Fragment>
    <NormalizeStyles />
    <BaseStyles />
    <Routes />
  </Fragment>
);

export default hot(App);

