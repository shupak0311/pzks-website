import { createGlobalStyle } from 'styled-components';

import { color, font, mixin } from '../shared/utils/styles';

export default createGlobalStyle`
  *,
  *::after,
  *::before {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }


  html, body, #root {
    height: 100%;
    min-height: 100%;
    font-family: "Roboto", sans-serif;
    font-size: 100%;
  }

  body {
    -webkit-tap-highlight-color: transparent;
    line-height: 1.2;
    overflow-x: hidden;
  }

  #root {
    display: flex;
    flex-direction: column;
  }

  *, *:after, *:before, input[type="search"] {
    box-sizing: border-box;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  ul {
    list-style: none;
  }

  ul, li, ol, dd, h1, h2, h3, h4, h5, h6, p {
    padding: 0;
    margin: 0;
  }

  button {
    background: none;
    border: none;
  }
`;
