import React, { useEffect } from 'react';
import { Router, Switch, Route, Redirect } from 'react-router-dom';
import { useTranslation } from "react-i18next";
import history from '../browserHistory';
import Project from '../Project/index';

const Routes = () => {
  const { i18n } = useTranslation();

  useEffect(() => {
    const language = localStorage.getItem("language");

    if (!language) localStorage.setItem("language", "ua");

    i18n
      .changeLanguage(language || "ua")
      .catch((err) => console.warn(err));
  }, []);

  return (
  <Router history={history}>
    <Switch>
      <Route path="/" component={Project} />
      <Route path="*" component={Project} />
      {/* <Route component={PageError} /> */}
    </Switch>
  </Router>
  )
}

export default Routes;
