import React from 'react';
import {render} from 'react-dom';
import i18n from './i18n';
import App from './App/index';

const root = document.createElement('div');
document.body.append(root);

// Register service worker
if ('serviceWorker' in navigator) {
	window.addEventListener('load', () => {
		navigator.serviceWorker.register('/sw.js').then(registration => {
			console.log('SW registered:', registration);
		}).catch(error => {
			console.log('SW registration failed:', error);
		});
	});
}

render(<App/>, root);
